/**
 *      @author Susmitha Rao
 *      @date   08/13/2015
        @description    Class for handling contact triggers.  
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Susmitha Rao                    08/13/2015          Original Version
        Radhika Bahl                    12/22/2015          Added check to exclude Supplier Contacts
               
 */
  public class JJ_Contact_trigger_handler {
  
      public static void createUserRecord(map<id,contact> mContacts){
         Set<Id> contactIds=new Set<Id>();
         //check for Employee and Non-Employee record types
         
         for(Contact oCon:mContacts.values()){
             if(oCon.recordTypeId == null || (!(oCon.recordTypeId==JJ_SRM_StaticConstants.SupplierContactRecordTypeId))){   
                 contactIds.add(oCon.Id);
             }
         } 
         JJ_Contact_User_sync.syncContactsUser(contactIds);      
      }
      
      public static void updateRecType(list<contact> lContacts){     
          for(contact oCon:lContacts){
              if(oCon.recordTypeId == null || (!(oCon.recordTypeId==JJ_SRM_StaticConstants.SupplierContactRecordTypeId))){   
              //Workday Load brings null value in record type, for Day 1. For Supplier Contacts, record type is Supplier, as its created thrugh UI.               
                  if(oCon.lastName==null)
                      oCon.lastName=oCon.firstName;
                  if(oCon.Worker_Is_Employee__c)
                       oCon.recordTypeId=JJ_StaticConstants.ContactRecTypeMapByName.get(JJ_StaticConstants.EmpRecType).getRecordTypeId();
                  else
                      oCon.recordTypeId=JJ_StaticConstants.ContactRecTypeMapByName.get(JJ_StaticConstants.NonEmpRecType).getRecordTypeId();
              }
          }        
      }
  
  
  }