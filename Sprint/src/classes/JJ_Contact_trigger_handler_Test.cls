@isTest(seeAllData=false)
private class JJ_Contact_trigger_handler_Test{
    

    /****
        Method Name : TestMethodData()
        Function: Used to set up the data for the test class
    ***/
    @testSetup static void TestMethodData() {
        
        
        contact newcontact = JJ_TestData.createContact('Ed','Employee');
        Account a = JJ_TestData.getAccount();
        List<Contact_User_Mapping__c> mappings1 = JJ_TestData.createContactUserMapping();
        insert mappings1;
        
        try{
            newContact.Worker_Is_Employee__c = true;
            Insert newcontact;
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.Label.Read_Out_Error)); 
        }
        user Testuser = JJ_TestData.createUser('strUn','System Administrator');
        try{
            Insert Testuser;
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.Label.Read_Out_Error)); 
        }     
    }

    
    /****
        Method Name : JJ_Contact_trigger_handler_Test()
        Function: Test method to validate Contact record type
    ***/
    
    static testMethod void JJ_Contact_trigger_handler_PositiveTest() {
        user Testuser = [select id, email  from user where email =: 'strUn@testjnj.org'];
        system.runas(Testuser){
            test.starttest();
            Contact C = [select id,WWID__c,LastName,FirstName,Phone,Email,Country_Name__c,Worker_Is_Employee__c,recordTypeId from contact where lastname = 'Employee'];
            JJ_Contact_trigger_handler.updateRecType(new List<Contact>{c});
            
            c.Worker_Is_Employee__c = false;
            c.LastName = null;
            system.assertequals(c.LastName,null );
            JJ_Contact_trigger_handler.updateRecType(new List<Contact>{c});
            Map<Id,Contact> contactMap = new Map<Id,Contact>();
            contactMap.put(c.Id,c);
            JJ_Contact_trigger_handler.createUserRecord(contactMap);
            Test.stoptest();
            
        }
        
    } 

}