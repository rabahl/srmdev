/**
 *      @author Susmitha Rao
 *      @date   11/09/2015
        @description    Class for handling event triggers.  
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Susmitha Rao                    11/0/2015          Original Version
               
 */
 public class JJ_Event_trigger_handler {     
     
     /*
    * @description: This  method is used to roll up planned and completed count of event to supplier Governance.
    * @param: List<event> 
    * @param: boolean
    * @return Type: void
    */
     public static void updateGovernanceMetrics(list<event> events,boolean isDel){
         
         //set to hold supplier ids
         set<id> accountIds= new set<id>();
         
         //set to hold years
         set<string> years=new set<string>();         
         set<string> yrAccIds=new set<string>();
         
         //set to hold deleted ids 
         set<id> delEventIds= new set<id>();  
         
         //list to supplier governance records for update
         list<Supplier_Governance__c> suppGovList=new list<Supplier_Governance__c>();
         
         //list to hold erro messages for tracking errors
         list<string> errMessages=new list<string>();  
          
             
         //get the supplier ids and deleted event ids
         for(event oEvent:events){
             if(oEvent.recordtypeId==JJ_SRM_StaticConstants.EventRecTypeMapByName.get(JJ_SRM_StaticConstants.SupplierEvent).getrecordTypeId()&&oevent.whatId<>null&&(Id.valueOf(oevent.whatId)).getSobjectType()==Schema.Account.SObjectType){
                 years.add(oEvent.year__c);
                 accountIds.add(oEvent.whatId);
                 yrAccIds.add(oEvent.year__c+oEvent.whatId);
             }
             if(isDel)
                 delEventIds.add(oevent.id);
                              
         }
         
         if(accountIds.size()>0){
             
             //get the accounts and related  suppier governance,event records  
             for(Account oAcc:[select id,(select whatId,Meeting_Status__c,Meeting_Type__c,Year__c from events where Year__c in :years and id not in :delEventIds),(Select Governance__r.name,Name,Supplier__c, year__c,Planned__c From Supplier_Governance__r where Year__c in :years) from account where id in :accountIds]){
                 
                 //map to hold planned count - supplier governance
                 map<string,integer> plannedCount=new map<string,integer>();
                 
                 //map to hold completed count - supplier governance
                 map<string,integer> completedCount=new map<string,integer>();
                 
                 //loop though events to calculate count
                 for(event oEvent:oAcc.events){
                     if(oEvent.Meeting_Type__c<>null){
                         list<string> meetingTypes=oEvent.Meeting_Type__c.split(';');
                        
                         if(meetingTypes.size()>0){
                             for(string meetingType:meetingTypes){
                                 if(oEvent.Meeting_Status__c=='Planned'){
                                     if(plannedCount.containskey(oEvent.Year__c+meetingType))
                                         plannedCount.put(oEvent.Year__c+meetingType,plannedCount.get(oEvent.Year__c+meetingType)+1);
                                     else
                                         plannedCount.put(oEvent.Year__c+meetingType,1);    
                                 
                                 }
                                 else if(oEvent.Meeting_Status__c=='Complete'){
                                     if(completedCount.containskey(oEvent.Year__c+meetingType))
                                         completedCount.put(oEvent.Year__c+meetingType,completedCount.get(oEvent.Year__c+meetingType)+1);
                                     else
                                         completedCount.put(oEvent.Year__c+meetingType,1);    
                                 
                                 }    
                                
                             }                       
                         
                         }                     
                     }                     
                 }
                 map<string,Supplier_Governance__c> yearGovMap=new map<string,Supplier_Governance__c>();                 
                 
                 //loop through supplier governance records and assign count
                 for( Supplier_Governance__c oSupGov:oAcc.Supplier_Governance__r){
                 
                     if(yrAccIds.contains(oSupGov.year__c+oSupGov.Supplier__c)){
                         if(plannedCount.containskey(oSupGov.year__c+oSupGov.Governance__r.name))
                             oSupGov.Planned__c=plannedCount.get(oSupGov.year__c+oSupGov.Governance__r.name); 
                         else
                             oSupGov.Planned__c=0;
                         if(completedCount.containskey(oSupGov.year__c+oSupGov.Governance__r.name))
                             oSupGov.JJ_Completed__c=completedCount.get(oSupGov.year__c+oSupGov.Governance__r.name);   
                         else
                             oSupGov.JJ_Completed__c=0;
                         SuppGovlist.add(oSupGov);
                     }
                 }
             }
             
             try{
                  //update supplier governance records
                 Database.SaveResult[] updateResults =database.update(SuppGovlist);
                 for(Database.SaveResult sr: updateResults ){
                     if(!sr.IsSuccess()){
                        for(Database.Error err : sr.getErrors()){
                            errMessages.add(err.getmessage());        
                        }
                      }
                  }                 
             }
             catch(exception e){
                 //log exceptions
                 JJ_SRM_ExceptionUtility.singleExceptionLog('JJ_Event_trigger_handler', '',e.getmessage());              
             }
             //log errors in exception log
             if(errMessages.size()>0)  
                 JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_Event_trigger_handler', '', errMessages);
         }         
     }
 }