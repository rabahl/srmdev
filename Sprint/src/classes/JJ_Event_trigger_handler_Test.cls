/* Class Name   : JJ_Event_trigger_handler_Test
  * Description   : This class is test class for JJ_Event_trigger_handler
  * Created By   : Susmitha
  * Created Date : 11/172015
  * Modification Log: 
  * --------------------------------------------------------------------------------------------------------------------------------------
  * Developer                Date                 Modification ID        Description 
  * ---------------------------------------------------------------------------------------------------------------------------------------
  * Susmitha               11/17/2015                                 Initial Version

  */
 @isTest(seeAllData=false)
  private class JJ_Event_trigger_handler_Test{
  private static User userRec;
    private static List<Account> Acclist =  new List<Account>();
    private static List<KPI_Master__c> MasterKPIlist =  new List<KPI_Master__c>();
    private static List<Supplier_Governance__c> Suppgovlist =  new List<Supplier_Governance__c>();
    private static Score_Card__c card;
    private static String fromDate;
    private static String toDate;
    private static List<Event> eventList=new List<Event>();
    private static List<Event> DeleteeventList=new List<Event>();
    private static List<Governance__c> Govlist =  new List<Governance__c>();
    
    private static testmethod void setup(){
        List<String> Triggerobjlist = new List<String>();
        Triggerobjlist.add('Event');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(Triggerobjlist);
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(3,3);
        GovList[0].name = 'Top to Top';
        GovList[1].name = 'Business Review';
        GovList[2].name = 'Category Strategy Review';
        GovList[3].name = 'Other (Supplier Related)'; 
        Update Govlist;      
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(1,3);
        Suppgovlist = [Select id, year__c from Supplier_Governance__c where Governance__c in : Govlist];
        system.assertequals(Suppgovlist[0].year__c,String.valueof(System.today().year()));
        //insert event
        Event eveObj=new Event(Subject='Email',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Planned', whatId=Acclist[0].Id, Meeting_Type__c='Top to Top;Other (Supplier Related)');
        eventList.add(eveObj);
        Event eveObj1=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Complete', whatId=Acclist[0].Id, Meeting_Type__c='Business Review;Category Strategy Review');
        eventList.add(eveObj1);
        Event eveObj2=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Cancelled', whatId=Acclist[0].Id, Meeting_Type__c='Scorecard Review');
        eventList.add(eveObj2);
        Event eveObj3=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Complete', whatId=Acclist[0].Id, Meeting_Type__c='Business Review;Category Strategy Review');
        eventList.add(eveObj3);
        Event eveObj4=new Event(Subject='Email',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Planned', whatId=Acclist[0].Id, Meeting_Type__c='Top to Top;Other (Supplier Related)');
        eventList.add(eveObj4);
        if(!eventList.isEmpty()){
            insert eventList;
            System.assertEquals(eventList.size(),5);
            eventList[1].Meeting_Status__c='Planned';
            update eventList[1];
            delete eventList;
                    
        }
        Delete GovList;   
        Event eveObj5=new Event(Subject='Email',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Planned', whatId=Acclist[0].Id, Meeting_Type__c='Top to Top;Other (Supplier Related)');
        DeleteeventList.add(eveObj5);
        Event eveObj6=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Complete', whatId=Acclist[0].Id, Meeting_Type__c='Business Review;Category Strategy Review');
        DeleteeventList.add(eveObj6);
        Event eveObj7=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Cancelled', whatId=Acclist[0].Id, Meeting_Type__c='Scorecard Review');
        DeleteeventList.add(eveObj7);
        Event eveObj8=new Event(Subject='Call',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Complete', whatId=Acclist[0].Id, Meeting_Type__c='Business Review;Category Strategy Review');
        DeleteeventList.add(eveObj8);
        Event eveObj9=new Event(Subject='Email',StartDateTime=System.Today(),EndDateTime=System.Today(), ActivityDate=System.today(),Meeting_Status__c='Planned', whatId=Acclist[0].Id, Meeting_Type__c='Top to Top;Other (Supplier Related)');
        DeleteeventList.add(eveObj9);
        if(!DeleteeventList.isEmpty()){
            insert DeleteeventList;                    
        }
    }
    
    private static testmethod void eventHandlerTest(){
        Test.startTest();
        setup();
        Test.stopTest();
    }
  
  }