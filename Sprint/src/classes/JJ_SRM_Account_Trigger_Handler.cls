/**************************************************************************************
Apex Class Name  : JJ_SRM_Account_Trigger_Handler
Version          : 1.0 
Created Date     : 14th October 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dhruv Bhalodi             14th October 2015        Original Version
* Susmitha                 11/30/2015                defaulted year to current year 
*************************************************************************************/
public class JJ_SRM_Account_Trigger_Handler{
    public static Id SuppRecordTypeId = JJ_SRM_StaticConstants.SupplierRecordTypeId;
    public static Integer InsertLimit = JJ_SRM_StaticConstants.InsertLimit;
    /*
    /*
    * @description: This  method is used to create Map of Segment against all governance in the system.
    * @param: None
    * @return Type: Map<String,List<Governance__c>>
    */
    
    public static Map<String,List<Governance__c>> createGovernanceSegmentMap(Set<String> AccountSegmentlst){
        Map<String,List<Governance__c>> govsegmentmap = new Map<String,List<Governance__c>>();
        List<Governance__c> templist;
        //List<Governance__c> governancelist = [Select Id, Name, Segment__c,Suggested_Cadence__c,Required__c  from Governance__c WHERE Segment__c in : AccountSegmentlst AND Required__c = 'Yes'];
        for(Governance__c gov : [Select Id, Name, Segment__c,Suggested_Cadence__c,Required__c  from Governance__c WHERE Segment__c in : AccountSegmentlst AND Required__c = 'Yes']){
            templist = new list<Governance__c>();
            if(govsegmentmap.containsKey(gov.Segment__c)){
                templist.add(gov);
                templist.addall(Govsegmentmap.remove(gov.Segment__c));
                govsegmentmap.put(gov.Segment__c,templist);
            }else{
                templist.add(gov);
                govsegmentmap.put(gov.Segment__c,templist);
            }
        }
        return govsegmentmap;
    }
    
    /*
    * @description: This  method is used to create Supplier Governance based on segmentation combination between supplier and Governance.
    * @param: List<Account>
    * @return Type: void
    */
    
    public static void CreateSupplierGovernance(List<Account> listNewValues) { 
        List<string> ListError = new List<string>();
        Map<String,List<Governance__c>> governancemap = New Map<String,List<Governance__c>>();
        List<Supplier_Governance__c> suppliergovlist = New List<Supplier_Governance__c>();
        Set<string> accountsegmentlst = new Set<string>();
        Supplier_Governance__c Suppliergov;
        List<Governance__c> listGovernance;
        for(Account acc : listNewvalues){
            Accountsegmentlst.add(acc.Segmentation__c);       
        }
            Governancemap = CreateGovernanceSegmentMap(Accountsegmentlst);
        for(Account acc : listNewValues){
            if(acc.recordtypeId == SuppRecordTypeId){
                try{
                   listGovernance = new List<Governance__c>();
                   if(Governancemap.containskey(acc.Segmentation__c)){
                       listGovernance.addall(Governancemap.get(acc.Segmentation__c));
                           for(Governance__c gov : listGovernance){
                                Suppliergov = New Supplier_Governance__c(); 
                                Suppliergov.Governance__c = gov.Id;
                                Suppliergov.Supplier__c = acc.Id;
                                Suppliergov.JJ_Target__c=gov.Suggested_Cadence__c;
                                Suppliergov.Year__c=String.valueOf(system.today().year());
                                Suppliergov.Governance_Unique_ID__c = gov.Name + String.valueOf(system.today().year()) + gov.Segment__c + acc.Id + gov.Id;
                                Suppliergovlist.add(Suppliergov);
                            }
                   }
                }
                catch(Exception e){
                    ListError.add(e.getmessage()+' and Line Number '+e.getLineNumber()+' Account : '+ acc + 'Governance records :'+ Governancemap.get(acc.Segmentation__c)); 
                }
           } 
        }
       if((suppliergovlist.size() < InsertLimit) && (!(suppliergovlist.isEmpty()))){
               Database.SaveResult[] SupplierGovernance = Database.Insert(Suppliergovlist, false); 
               for(Database.SaveResult cnt : SupplierGovernance){
                    if(!cnt.IsSuccess()){
                        for(Database.Error err : cnt.getErrors()){
                                    ListError.add(err.getmessage());        
                        }
                    }
               }
       } else if(Suppliergovlist.size() > InsertLimit){
           ListError.add('The Number of records processed where more than 10000. Please contact the Administrator');   
       }
       
       if(!(ListError.isEmpty()))  
           JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_Account_Trigger_Handler', '', ListError);
    }
    
    
}