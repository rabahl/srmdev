/* Class Name   : JJ_SRM_Account_Trigger_Handler_Test
  * Description   : This class is test class for JJ_Account_Trigger_Handler
  * Created By   : Bhalodi Dhruv
  * Created Date : 15-Oct-2015
  * Modification Log: Modified by Bhalodi Dhruv on 15-Oct-2015
  * --------------------------------------------------------------------------------------------------------------------------------------
  * Developer                Date                 Modification ID        Description 
  * ---------------------------------------------------------------------------------------------------------------------------------------
  * Bhalodi Dhruv          15-Oct-2015                                 Initial Version

  */

@isTest
private class JJ_SRM_Account_Trigger_Handler_Test{

    private static User userRec;
    private static List<Governance__c> Govlist =  new List<Governance__c>();
    private static List<Account> Acclist =  new List<Account>();
    /*
    * @description: This method is used for creating test data used in test class
    * @param: None
    * @return Type: void
    */
    private static void setUpDataPositive(){
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(3,3);  
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(3,3);    
    }
    
    private static void setUpDataBulk(){
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(10,10);
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(10,10);
    }
    
    private static void setUpDataNegative(){
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(0,100);
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(0,100);
    }
    
     /*
    * @description: This method is used for testing positive functionality in JJ_Account_Trigger_Handler
    * @param: None
    * @return Type: void
    */
    private static testMethod void positive_Test_Method(){
        Test.startTest();
        setUpDataPositive();    
           System.runAs(userRec){
               List<Supplier_Governance__c> SuppGov = [Select Id, Governance__c, Supplier__c from Supplier_Governance__c  where Governance__r.name LIKE 'foo gov%'];
               if(!SuppGov.isEmpty()){
                   system.assertequals(SuppGov.size(),12);
               }
           }
        Test.stopTest();     
    }
    
    /*
    * @description: This method is used for testing negative functionality in JJ_Account_Trigger_Handler
    * @param: None
    * @return Type: void
    */   
    private static testMethod void negative_Test_Method(){
        Test.startTest();
        setUpDataNegative();    
           System.runAs(userRec){
               List<Supplier_Governance__c> SuppGov = [Select Id, Governance__c, Supplier__c from Supplier_Governance__c where Governance__r.name LIKE 'foo gov 1%'];
               system.assertequals(SuppGov.size(),0);
           }
        Test.stopTest();     
    }
    
    /*
    * @description: This method is used for testing Bulk functionality in JJ_Account_Trigger_Handler
    * @param: None
    * @return Type: void
    */   
    private static testMethod void bulk_Test_Method(){
        Test.startTest();
        setUpDataBulk();    
           System.runAs(userRec){
           List<Supplier_Governance__c> SuppGov = [Select Id, Governance__c, Supplier__c from Supplier_Governance__c where Governance__r.name LIKE 'foo gov%'];
               if(!SuppGov.isEmpty()){
                   system.assertequals(SuppGov.size(),110);
               }
           }
        Test.stopTest();     
    }
}