/**************************************************************************************
Apex Class Name  : JJ_SRM_Contact_Trigger_Handler
Version          : 1.0 
Created Date     : 04th January 2016
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dhruv Bhalodi             04th January 2016        Original Version
*************************************************************************************/
public class JJ_SRM_Contact_Trigger_Handler{
    /*
    /*
    * @description: This  method is used to change contact owner to account owner on creation by External Supplier.
    * @param: List<Contact>
    * @return Type: None...
    */
    
    public static void ChangeContactOwner(List<Contact> listNewValues) { 
    List<Id> ChangeContactOwnerList = New List<Id>();
    Map<Id,Id> AccountOwnerMap = New Map<Id,Id>();
    Set<Id> setAccountId = New Set<Id>();
    for(Contact Cnt : listNewValues){
        setAccountId.add(Cnt.AccountId);    
    }
    for(Account Acc : [Select Id, OwnerId from Account where Id in : setAccountId]){
        If(!(AccountOwnerMap.containskey(Acc.Id))){
            AccountOwnerMap.put(Acc.Id,Acc.OwnerId);    
        }            
    }
    for(Contact Cnt : listNewValues){
        if(AccountOwnerMap.get(Cnt.AccountId) != null){
            Cnt.OwnerId = AccountOwnerMap.get(Cnt.AccountId);  
        }
    }
    }
}