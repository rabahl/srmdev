@isTest(SeeAllData=false)
private class JJ_SRM_Contact_Trigger_Handler_Test{
    private static Contact con;
    private static Contact conExternal;
    private static User portaluserrec;
    private static User userrec;
    private static List<Account> accList = new List<Account>();
    
    static void testSetupUser(){
        userrec = JJ_SRM_TestDataSupplier.createUser('test','System Administrator');         
        userrec.userroleid = [Select Id from userrole where name =: 'Manila Vitals Specialist' LIMIT 1].id;
    }
    static void testSetupPortalUser() {
        //Creating User to run in test user mode.        
        portaluserrec = JJ_SRM_TestDataSupplier.createUser('portaltestuser','SRM Supplier Portal User');         
        portaluserrec.ContactId = con.id; 
    }
    
    static void testSetupContact() {
      List<String> objlist = new List<String>();
      objlist.add('Contact');
      JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
      con= JJ_SRM_TestDataSupplier.createContact('asd1','qwe1');
      con.WWID__c = '53545'+Math.random();
      con.WWID__c = con.WWID__c.left(10);
      String contactEmail=con.Email;
      con.Email = Math.random()+'';
      con.Email = con.Email.left(4);
      con.Email = con.Email+contactEmail;
      con.RecordTypeId = JJ_SRM_StaticConstants.SupplierContactRecordTypeId;
      for(Account acc : accList ){
           con.AccountId = acc.Id;
       }
       insert con; 
    }
    
    static void testSetupAccount(){
       accList = JJ_SRM_TestDataSupplier.CreateAccount(1,0); 
    } 
    
    @future
    static void testcontactmapping() {
     List<Contact_User_Mapping__c> mappings1 = JJ_TestData.createContactUserMapping();
     insert mappings1;
    } 
    
    /****
        Method Name : testFunc()
        Function: Used to test ChangeContactOwner() method of JJ_SRM_Contact_Trigger_Handler class
    ***/  
    static testMethod void testFunc(){
        testSetupUser();
        System.runAs(userrec){
           testcontactmapping();
           testSetupAccount();
           testSetupContact();
           testSetupPortalUser(); 
        }      
        
       System.runAs(portaluserrec){              
            Test.startTest();        
            testSetupContact();
            Contact contactrec = [select Id,ownerId,owner.Name from Contact where Id = :con.Id];
            System.AssertEquals(contactrec.owner.Name,'test User');                                             
            Test.stopTest();                        
        }
        
    }


}