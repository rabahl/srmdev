/*********************************************************************************
Class Name      : JJ_SRM_CreateGovernanceBatch
Description     : This class is used to execute and create governance items for next year
Created By      : Dhruv Bhalodi
Created Date    : 12-Dec-14
Modification Log: Last modified by Dhruv Bhalodi
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Dhruv Bhalodi           12-Dec-14              Initial Version
*********************************************************************************/

global with sharing class JJ_SRM_CreateGovernanceBatch implements Database.Batchable<Sobject>, Database.AllowsCallouts, Database.Stateful {   

    public List<Id> SelectedIDs = new List<Id>();
    global List<string> ListError = new List<string>();
    public List<Governance__c> ListGov = new List<Governance__c>();
    public Set<String> Segmentsselected = new set<String>();
    public static Id SuppRecordTypeId = JJ_SRM_StaticConstants.SupplierRecordTypeId;   
    
    /*********************************************************************************
    Method Name    : JJ_SRM_CreateGovernanceBatch
    Description    : Constructor
    Return Type    : void
    Parameter      : 1. GovernanceSelected : List of ID selected               
    *********************************************************************************/
   
    global JJ_SRM_CreateGovernanceBatch()
    {
        
        ListGov = [Select Id, Name, Segment__c,Suggested_Cadence__c,Last_Batch_Run__c,Lastmodifieddate from Governance__c where Required__c = 'Yes'];
        for(Governance__c gov : ListGov){
            Segmentsselected.add(gov.Segment__c); 
        }
    }
    /*********************************************************************************
    Method Name    : start
    Description    : Start method to fetch the XMLS
    Return Type    : Iterable<String>
    Parameter      : 1. BC : Context for the batch                
    *********************************************************************************/
   
    global Iterable<Sobject> start(Database.BatchableContext BC){
        String query = 'Select Id,Segmentation__c from Account where Segmentation__c in : Segmentsselected AND Active__c = True AND RecordtypeID = : SuppRecordTypeId ';
        return database.query(query);
    }
    /*********************************************************************************
    Method Name    : execute
    Description    : Execute method to process the list of Governance and create Supplier Governance
    Return Type    : void
    Parameter      : 1. BC : Context for the batch 
                     2. lstgovernance : List of Governance               
    *********************************************************************************/
    global void execute(Database.BatchableContext BC,  List<Account> lstAccount){
    list<Supplier_Governance__c> listSuppgov = new List<Supplier_Governance__c>();
    Supplier_Governance__c Suppliergov;
    Schema.SObjectField Uniquefield = Supplier_Governance__c.Fields.Governance_Unique_ID__c;
    for(Account acc : lstAccount){
        for(Governance__c gov : ListGov){
            if(gov.Segment__c == acc.Segmentation__c){
                Suppliergov = New Supplier_Governance__c(); 
                Suppliergov.Governance__c = gov.Id;
                Suppliergov.Supplier__c = acc.Id;
                Suppliergov.Year__c=String.valueOf(system.today().year()+1);
                Suppliergov.Governance_Unique_ID__c =  gov.Name + String.valueOf(system.today().year()+1) + gov.Segment__c + acc.Id + gov.Id;
                listSuppgov.add(Suppliergov);    
            }  
        }  
    }
    
    Database.SaveResult[] SuppGovlist= Database.Insert(listSuppgov,false);
    for(Database.Saveresult Suppgov : SuppGovlist){
        if(!Suppgov.IsSuccess()){
            for(Database.Error err : Suppgov.getErrors()){
                ListError.add(err.getmessage());        
            }
        }
    } 
    }
    

    /*********************************************************************************
    Method Name    : finish
    Description    : finish method 
    Return Type    : void
    Parameter      : 1. BC : Context for the batch                         
    *********************************************************************************/
    global void finish(Database.BatchableContext BC){
        if(!(ListError.isEmpty())){
            JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_SRM_CreateGovernance', '', ListError);
        }
        else{
            for(Governance__c Gov : ListGov){
                Gov.Last_Batch_Run__c = System.now();
            }
            Database.SaveResult[] Govlist= Database.update(ListGov,false);
        }     
    }

}