/*********************************************************************************
Class Name      : JJ_SRM_CreateGovernanceBatch_Execution
Description     : This class is used to execute Batch JJ_SRM_CreateGovernanceBatch and create governance items for next year from bacth
Created By      : Dhruv Bhalodi
Created Date    : 12-Dec-15
Modification Log: Last modified by Dhruv Bhalodi
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Dhruv Bhalodi           12-Dec-15             Initial Version
*********************************************************************************/

global with sharing class JJ_SRM_CreateGovernanceBatch_Execution{ 

 webservice static void executegovernancecreationBatch() {             
  //Calling Constructor for Batch Execution       
  Database.executebatch(new  JJ_SRM_CreateGovernanceBatch(),200);
 }
 
 webservice static Boolean LoggedinUserPermission() {             
  //Calling method to verify permission set assignment for logged in user    
 List<PermissionSetAssignment> Assignedsets = [Select Id from PermissionSetAssignment where PermissionSetId = : Label.Business_Owner_Permission_Set_ID AND AssigneeId = : Userinfo.getuserid()];  
 if(Assignedsets.isempty())
     return False;
 else
     return True;
 }
}