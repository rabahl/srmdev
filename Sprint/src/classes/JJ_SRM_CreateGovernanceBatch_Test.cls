/* Class Name   : JJ_SRM_CreateGovernanceBatch_Test
  * Description   : This class is test class for JJ_Account_Trigger_Handler
  * Created By   : Bhalodi Dhruv
  * Created Date : 08-Dec-2015
  * Modification Log: Modified by Bhalodi Dhruv on 15-Oct-2015
  * --------------------------------------------------------------------------------------------------------------------------------------
  * Developer                Date                 Modification ID        Description 
  * ---------------------------------------------------------------------------------------------------------------------------------------
  * Bhalodi Dhruv          08-Dec-2015                                 Initial Version

  */

@isTest
private class JJ_SRM_CreateGovernanceBatch_Test{

    private static User userRec;
    private static List<Governance__c> Govlist =  new List<Governance__c>();
    private static List<Account> Acclist =  new List<Account>();
    /*
    * @description: This method is used for creating test data used in test class
    * @param: None
    * @return Type: void
    */
    private static void setUpDataPositive(){
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(3,3);  
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(3,3);    
    }
    
    private static void setUpDataNegative(){
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        //Govlist = JJ_SRM_TestDataSupplier.CreateGovernance(0,100);
        //Acclist = JJ_SRM_TestDataSupplier.CreateAccount(0,100);
    }
    
     /*
    * @description: This method is used for testing positive functionality in JJ_Account_Trigger_Handler
    * @param: None
    * @return Type: void
    */
    private static testMethod void positive_Test_Method(){
        
        setUpDataPositive();    
           System.runAs(userRec){
           Test.startTest();
           //Govlist = [Select id from Governance__c where name LIKE 'foo gov%'];
           List<Id> GovIDList = new list<Id>();
           for(Governance__c GovID : [Select id from Governance__c where name LIKE 'foo gov%']){
               GovIDList.add(GovID.Id);
           }
           JJ_SRM_CreateGovernanceBatch_Execution.executegovernancecreationBatch();
           Test.stopTest();  
           List<Supplier_Governance__c> SuppGov = [Select Id, Governance__c, Supplier__c from Supplier_Governance__c where Governance__r.name LIKE 'foo gov%'];            
           system.assertequals(SuppGov.size(),24);
           }
          
    }
    
    /*
    * @description: This method is used for testing negative functionality in JJ_Account_Trigger_Handler
    * @param: None
    * @return Type: void
    */   
    private static testMethod void negative_Test_Method(){
        
        setUpDataNegative();    
           System.runAs(userRec){
           Test.startTest();
           //Govlist = [Select id from Governance__c where name LIKE 'foo gov%'];
           List<Id> GovIDList = new list<Id>();
           for(Governance__c GovID : [Select id from Governance__c where name LIKE 'foo gov%']){
               GovIDList.add(GovID.Id);
           }
           JJ_SRM_CreateGovernanceBatch_Execution.executegovernancecreationBatch();
           JJ_SRM_CreateGovernanceBatch_Execution.LoggedinUserPermission();
           Test.stopTest();
           List<Supplier_Governance__c> SuppGov = [Select Id, Governance__c, Supplier__c from Supplier_Governance__c where Governance__r.name LIKE 'foo gov%'];                
           system.assertequals(SuppGov.size(),0);
           }
        
    }
}