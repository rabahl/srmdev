/*****************************************************************************************************
Apex Class Name  : JJ_SRM_Event_OtherAgendaTopicsController 
Version          : 1.0 
Created Date     : 11th March 2016
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------------------------
* Niranjan Unnamatla          03/11/2016             To modify the Other Agenta Topics field on Event 
******************************************************************************************************/
public class JJ_SRM_Event_OtherAgendaTopicsController {

    Public Event EventRecord {get; set;}
    Id EventId;
    
    public JJ_SRM_Event_OtherAgendaTopicsController(ApexPages.StandardController controller) {
        EventId = Controller.getRecord().id;
        EventRecord = [ Select Id, Agenda_Topics_Information__c from Event where Id =: EventId ];
    }
    
    public void AddOtherAgendaTopics(){
        try{
            update EventRecord ;
        }
        catch(exception ex){
            JJ_SRM_ExceptionUtility.singleExceptionLog('JJ_SRM_Event_OtherAgendaTopicsController', '', ex.getmessage() + ' and Line Number'+ ex.getLineNumber());
        }
    }
}