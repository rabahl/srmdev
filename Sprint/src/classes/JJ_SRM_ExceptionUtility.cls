/*********************************************************************************
Class Name      : JJ_SRM_ExceptionUtility
Description     : This class is used to Log Exceptions in Exception Log object
Created By      : Dhruv Bhalodi
Created Date    : 15-Oct-14
Modification Log: Last modified by Dhruv Bhalodi
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Dhruv Bhalodi             15-Oct-14              Initial Version
*********************************************************************************/

global class JJ_SRM_ExceptionUtility{

    public static void singleExceptionLog(string className, string pageName, string exceptionMessage){       
        Exception_Log__c exc= new Exception_Log__c();
        exc.class_Name__c = className;
        exc.page_Name__c = pageName;
        exc.Detailed_Exception__c = exceptionMessage;
        exc.Exception_Date_Time__c = system.now();
        exc.Running_User__c = UserInfo.getUserId();   
        if(exc!=null){     
            insert exc;   
        }
    }
    
    public static void multipleExceptionLog(string className, string pageName, List<String> ListError){
    List<Exception_Log__c> InsertException = new List<Exception_Log__c>();  
    Exception_Log__c exc;
        for(String ExceptionMessage : ListError){
            exc = new Exception_Log__c();
            exc.class_Name__c = className;
            exc.Page_Name__c = pageName;
            exc.Detailed_Exception__c = ExceptionMessage;
            exc.Exception_Date_Time__c = system.now();
            exc.Running_User__c = UserInfo.getUserId();  
            InsertException.add(exc);  
        }
        Database.SaveResult[] ExcErrorList = Database.Insert(InsertException, false);
    }
}