/* Class Name   : JJ_SRM_ExceptionUtility_Test
  * Description   : This class is test class for JJ_SRM_ExceptionUtility
  * Created By   : Bhalodi Dhruv
  * Created Date : 11-Nov-2015
  * Modification Log: Modified by Bhalodi Dhruv on 11-Nov-2015
  * --------------------------------------------------------------------------------------------------------------------------------------
  * Developer                Date                 Modification ID        Description 
  * ---------------------------------------------------------------------------------------------------------------------------------------
  * Bhalodi Dhruv          11-Nov-2015                                 Initial Version

  */

@isTest
private class JJ_SRM_ExceptionUtility_Test{
     /*
    * @description: This method is used for testing functionality in JJ_SRM_ExceptionUtility
    * @param: None
    * @return Type: void
    */
    private static testMethod void test_Method(){
        Test.startTest();
           JJ_SRM_ExceptionUtility.SingleExceptionLog('Test_Utiltiy','','Test Class exception Insert');
           List<Exception_Log__c> exceptionlog = [Select Id from Exception_Log__c where class_Name__c ='Test_Utiltiy'];
           if(!exceptionlog.isEmpty()){
               system.assertequals(exceptionlog.size(),1);
           }
        Test.stopTest();     
    }
}