public with sharing class JJ_SRM_Reference_Materials_Controller
{
    public PageReference RedirecttoKnowledge(){
        pagereference pg=new pagereference('/_ui/knowledge/ui/KnowledgeHome');
        pg.setredirect(true);
        return pg;  
    }
 
}