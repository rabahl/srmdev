@isTest(seeALLData=false)
private class JJ_SRM_RelatedTracking_Tgr_Handler_Test{

    private static List<Action_Item_Log__c> tracklog= new List<Action_Item_Log__c>();
    private static Related_Action_Log__c RTL;
    static void TestMethodData(){
        List<String> objlist = new List<String>();
        objlist.add('Related_Action_Log__c');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        tracklog = JJ_SRM_TestDataSupplier.CreateTrackingLog(3);  
        RTL = new Related_Action_Log__c();
        RTL.Action_Log_Item_1__c = tracklog[0].Id;
        RTL.Action_Log_Item_2__c = tracklog[1].Id;  
        if(RTL!=null){
            insert RTL;
        }
    }
    
    static testMethod void getScoreCardTest() {
    Test.startTest();
    TestMethodData();
    
    List<Related_Action_Log__c> lstRTL = [Select Related_Action_Log__c.Action_Log_Item_1__c from Related_Action_Log__c WHERE Action_Log_Item_2__c =:tracklog[0].Id];
    system.assertequals(lstRTL.size(),1);
    system.assertequals(lstRTL[0].Action_Log_Item_1__c,tracklog[1].Id);
    RTL.Action_Log_Item_2__c = tracklog[2].Id;
    if(RTL!=null){
        Update RTL;
    }
    //List<Related_Tracking_Log__c> lstRTL2 = [Select Related_Tracking_Log__c.Action_Log_Item_1__c from Related_Tracking_Log__c WHERE Action_Log_Item_2__c =:tracklog[0].Id];

   // system.assertequals(lstRTL2.size(),1);
   // RTL.Tracking_Log_Item_2__c = tracklog[1].Id;
    //Assert
    if(RTL!=null){
        Delete RTL;
    }
    //Assert
    Test.stopTest();
    }
    
    }