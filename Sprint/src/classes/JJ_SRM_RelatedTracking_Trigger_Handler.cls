/*Apex Class Name  : JJ_SRM_RelatedTracking_Trigger_Handler
Version          : 1.0 
Created Date     : 26th October 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Nirmal Gupta             26th October 2015        Original Version
*************************************************************************************/
public class JJ_SRM_RelatedTracking_Trigger_Handler{
   
    public List<string> ListError = new List<string>();
    
    /*
    * @description: This  method is used to handle Insert events
    * @param: None
    * @return Type: void
    */
    
    public void InsertHandler_RelatedTracking(List<Related_Action_Log__c> trackingloglst ){
        List < Related_Action_Log__c > NewRTLList = new List < Related_Action_Log__c > ();
        for (Related_Action_Log__c RTL: trackingloglst) {
            If(RTL.Parent_Related_Action_Log__c == null && RTL.Action_Log_Item_1__c != null && RTL.Action_Log_Item_2__c != null) {
               Related_Action_Log__c newRTL = new Related_Action_Log__c();
                newRTL.Action_Log_Item_1__c = RTL.Action_Log_Item_2__c;
                newRTL.Action_Log_Item_2__c = RTL.Action_Log_Item_1__c;
                newRTL.Parent_Related_Action_Log__c = RTL.Id;
                NewRTLList.add(newRTL);
            }

        }

        if (!(NewRTLList.isEmpty())) {
            Database.insert(NewRTLList);
            List < Related_Action_Log__c> UpdateRTLList = new List < Related_Action_Log__c > ();
            for (Related_Action_Log__c uRTL: NewRTLList) {
                if (Trigger.newMap.get(uRTL.Parent_Related_Action_Log__c) != null && uRTL.Id != null) {
                    Related_Action_Log__c updateRTL = new Related_Action_Log__c();
                    updateRTL.Id = Trigger.newMap.get(uRTL.Parent_Related_Action_Log__c).Id;
                    updateRTL.Parent_Related_Action_Log__c = uRTL.Id;
                    UpdateRTLList.add(updateRTL);
                }

            }

            if (!(UpdateRTLList.isEmpty())){
                Database.SaveResult[] resultList = Database.update(UpdateRTLList);
                for(Database.SaveResult result : resultList){
                    if(!result.IsSuccess()){
                        for(Database.Error err : result.getErrors()){
                            ListError.add(err.getmessage());        
                        }
                    }
                }
            }
        }
        if(!(ListError.isEmpty())){ 
           JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_SRM_RelatedTracking_Trigger_Handler', '', ListError);
        }
    }
    
    /*
    * @description: This  method is used to handle Delete events
    * @param: None
    * @return Type: void
    */
    
    public void DeleteHandler_RelatedTracking(List<Related_Action_Log__c> trackinglogOldlst){
        List < Related_Action_Log__c > deleteRTLList = new List < Related_Action_Log__c > ();
        Set < Id > deleteRTLIdSet = new Set < Id > ();

        for (Related_Action_Log__c RTL: trackinglogOldlst) {
            deleteRTLIdSet.add(RTL.Parent_Related_Action_Log__c);
        }

        if (!(deleteRTLIdSet.isEmpty())) {
            deleteRTLList = [select Id from Related_Action_Log__c where id in : deleteRTLIdSet];
            //if (deleteRTLList.size() > 0){
            if (!deleteRTLList.isEmpty()){                
                Database.deleteResult[] resultList = Database.delete(deleteRTLList);
                for(Database.deleteResult result : resultList){
                    if(!result.IsSuccess()){
                        for(Database.Error err : result.getErrors()){
                            ListError.add(err.getmessage());        
                        }
                    }
                }
            }
        }
        if(!(ListError.isEmpty())){ 
           JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_SRM_RelatedTracking_Trigger_Handler', '', ListError);
        }
    }
    
    /*
    * @description: This  method is used to handle update events
    * @param: None
    * @return Type: void
    */
    
    public void UpdateHandler_RelatedTracking(List<Related_Action_Log__c> trackingloglst, Map<Id,Related_Action_Log__c> Oldmap, Map<Id,Related_Action_Log__c> Newmap){
        List < Related_Action_Log__c > editRTLList = new List < Related_Action_Log__c > ();
        Set < Id > editRTLIdSet = new Set < Id > ();
        for (Related_Action_Log__c editRTL: trackingloglst) {
            if (editRTL.Action_Log_Item_1__c != Oldmap.get(editRTL.Id).Action_Log_Item_1__c || editRTL.Action_Log_Item_2__c != Oldmap.get(editRTL.Id).Action_Log_Item_2__c) {
                editRTLIdSet.add(editRTL.Parent_Related_Action_Log__c);
            }
        }
        if (!(editRTLIdSet.isEmpty())) {
            for (Related_Action_Log__c editParentRTL: [select Id, Parent_Related_Action_Log__c, Action_Log_Item_2__c, Action_Log_Item_1__c from Related_Action_Log__c where Id in : editRTLIdSet]) {
                if (Trigger.newMap.get(editParentRTL.Parent_Related_Action_Log__c) != null) {
                    editParentRTL.Action_Log_Item_1__c = Newmap.get(editParentRTL.Parent_Related_Action_Log__c).Action_Log_Item_2__c;
                    editParentRTL.Action_Log_Item_2__c = Newmap.get(editParentRTL.Parent_Related_Action_Log__c).Action_Log_Item_1__c;
                    editRTLList.add(editParentRTL);
                }
            }
            if (!(editRTLList.isEmpty())){                
                Database.SaveResult[] resultList = Database.update(editRTLList);
                for(Database.SaveResult result : resultList){
                    if(!result.IsSuccess()){
                        for(Database.Error err : result.getErrors()){
                            ListError.add(err.getmessage());        
                        }
                    }
                }
            }
        }
        if(!(ListError.isEmpty())){  
           JJ_SRM_ExceptionUtility.MultipleExceptionLog('JJ_SRM_RelatedTracking_Trigger_Handler', '', ListError);
        }
    }
  
}