/**
*      @author Radhika Bahl
*      @date   24/11/2015
       @description  class for declaring static constants for SRM project

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------    

**/

public class JJ_SRM_StaticConstants{

    //Contact Record type map
    //public static String SupplierContact = 'Supplier Contact';
    public static String SupplierContact = System.Label.JJ_SRM_SupplierContact;
    public static Id SupplierContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(SupplierContact).getRecordTypeId();
       
     // Account Record Type
    //public static String Supplierrecordtype = 'Supplier';
    public static String Supplierrecordtype = System.Label.JJ_SRM_Supplier;
    public static Id SupplierRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Supplierrecordtype).getRecordTypeId();
    
    //Event record type
    public static String SupplierEvent = System.Label.JJ_SRM_Supplier_Event;
    public static map<string,Schema.RecordTypeInfo> EventRecTypeMapByName=schema.Sobjecttype.Event.getRecordTypeInfosByName();
    //public static String SupplierEvent = 'Supplier Event';
     
    // Limit for Supplier Governance Insert
    public static Integer InsertLimit = 9999;
}