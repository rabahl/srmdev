/**
*      @author Radhika Bahl
*      @date   12/11/2015
       @description  Created for US-5053 and US-5215, to show KPI Scores when user clicks on View Score link as a pop-up window

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Radhika Bahl                  12/11/2015         Created    
*/
public with sharing  class JJ_SRM_SupplierKPIScorePopup{
    
    //variable & Collections declaration
    List<Supplier_KPI__c> listSupplierKPI=new List<Supplier_KPI__c>();
    public String kpiName{get;set;}
    public String unit{get;set;}
    public String frequency{get;set;}
    public String evaluationPeriod {get;set;}
    public Set<Supplier_KPI_Score__c> setKpiScores;
    public List<Supplier_KPI_Score__c> listKpiScores{get; set;}
    public List<Supplier_KPI_Score__c> finalListKpiScores{get; set;}
    public String supplierKPIMasterName;
    public String accountId;
    public Date fromDate;
    public Date toDate;
    public Integer fromMonth;
    public Integer toMonth;
    public String fromYear;
    public String toYear;
    
    public JJ_SRM_SupplierKPIScorePopup(){
    
        finalListKpiScores=new List<Supplier_KPI_Score__c>();
        supplierKPIMasterName=ApexPages.currentPage().getParameters().get('name');
        accountId=ApexPages.currentPage().getParameters().get('accountId');
        if(ApexPages.currentPage().getParameters().get('FromDate')!='' && ApexPages.currentPage().getParameters().get('FromDate')!=null){
            String[] strDate=(ApexPages.currentPage().getParameters().get('FromDate')).split('/');
            Integer myIntDate = integer.valueOf(strDate[1]);
            Integer myIntMonth = integer.valueOf(strDate[0]);
            Integer myIntYear = integer.valueOf(strDate[2]);
            fromDate = Date.newInstance(myIntYear, myIntMonth, myIntDate);
            fromMonth=fromDate.month();
            fromYear=String.valueOf(fromDate.Year());
        }
        if(ApexPages.currentPage().getParameters().get('ToDate')!=null && ApexPages.currentPage().getParameters().get('ToDate')!=''){
            String[] strDate=(ApexPages.currentPage().getParameters().get('ToDate')).split('/');
            Integer myIntDate = integer.valueOf(strDate[1]);
            Integer myIntMonth = integer.valueOf(strDate[0]);
            Integer myIntYear = integer.valueOf(strDate[2]);
            toDate= Date.newInstance(myIntYear, myIntMonth, myIntDate);
            toMonth=toDate.month();
            toYear=String.valueOf(toDate.Year());
        }
        //calling method to populate list of Scores
        try{
            finalListKpiScores=processKPIAndScores(supplierKPIMasterName,accountId,fromYear,toYear,fromMonth,toMonth);
        }catch(Exception ex){
            JJ_SRM_ExceptionUtility.singleExceptionLog('JJ_SRM_SupplierKPIScorePopup', '', ex.getmessage()+' and Line Number'+ex.getLineNumber());
        }
    }
    
    /*
    * @description: This  method is used to process all KPIs and their corresponding scores based on From and To Date
    * @param: String,String,String,String
    * @return Type: None
    */  
    public List<Supplier_KPI_Score__c> processKPIAndScores(String supplierKPIMasterName,String accountId,String fromYear,String toYear, Integer fromMonth, Integer toMonth){
        //populate Supplier KPI information
        //there can be multiple Supplier KPIs on a Account, with same KPI Master name but would be for different evaluation period   
        listKpiScores =new List<Supplier_KPI_Score__c>();
        listSupplierKPI=[select name, Unit__c,Frequency__c,Evaluation_Period__c,(select Supplier_KPI__r.name,Supplier_KPI__r.Frequency__c,Supplier_KPI__r.Unit__c,Supplier_KPI__r.Evaluation_Period__c,name,Locked__c ,RAG__c,Score__c,Period__c,Period_Calculation__c from KPI_Scores__r) from Supplier_KPI__c where KPI_Master__r.name=:supplierKPIMasterName AND Supplier_Name__c=:accountId and (Evaluation_Period__c=:fromYear OR Evaluation_Period__c=:toYear)];
        if(!listSupplierKPI.isEmpty()){
            kpiName=supplierKPIMasterName;
            unit='';
            frequency='';
            frequency= frequency+ listSupplierKPI[0].Frequency__c;
            unit=unit + listSupplierKPI[0].Unit__c;
            //there can be maximum two KPI records for cross-year scenario, so concatenating frequencies and units
            if(listSupplierKPI.size()>1 && listSupplierKPI[1]!=null){
                if(listSupplierKPI[1].Frequency__c!=listSupplierKPI[0].Frequency__c){
                    frequency= frequency+ ','+ listSupplierKPI[1].Frequency__c;
                }
                if(listSupplierKPI[1].Unit__c!=listSupplierKPI[0].Unit__c){
                    unit=unit + ','+listSupplierKPI[1].Unit__c;
                }
            }
            listKpiScores =new List<Supplier_KPI_Score__c>();
            //filter out KPI Scores based upon from and to date on Scorecard
            for(Supplier_KPI__c kpi:listSupplierKPI){
            
                setKpiScores =new Set<Supplier_KPI_Score__c>();
                for(Supplier_KPI_Score__c s:(List<Supplier_KPI_Score__c>)kpi.KPI_Scores__r){
                    if(kpi.Frequency__c=='Monthly'){
                            if((s.Supplier_KPI__r.Evaluation_Period__c == fromYear && s.Period_Calculation__c == fromMonth) || (s.Supplier_KPI__r.Evaluation_Period__c == toYear && s.Period_Calculation__c == toMonth)){
                            setKpiScores.add(s);
                        }
                        if((fromYear==toYear) && s.Period_Calculation__c>=fromMonth && s.Period_Calculation__c<=toMonth){
                            setKpiScores.add(s);
                        }
                        else if((fromYear!=toYear) && ((s.Period_Calculation__c>=fromMonth && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (s.Period_Calculation__c<=toMonth && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            setKpiScores.add(s);
                        }
                    }else if(kpi.Frequency__c=='Quarterly'){
                        //Q1
                        if( (fromYear==toYear) && ((fromMonth >=1 && fromMonth <=3) || (toMonth>=1 && toMonth<=3))){
                            if(s.Period_Calculation__c==13){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=3 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=1 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==13){
                                setKpiScores.add(s);
                            }
                        }
                        //Q2
                        if((fromYear==toYear) && ((fromMonth >=4 && fromMonth <=6) || (toMonth>=4 && toMonth<=6) || (fromMonth <=4 && toMonth>=6))){
                            if(s.Period_Calculation__c==14){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=6 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=4 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==14){
                                setKpiScores.add(s);
                            }
                        }
                        //Q3
                        if((fromYear==toYear) && ((fromMonth >=7 && fromMonth <=9) || (toMonth>=7 && toMonth<=9) || (fromMonth <=7 && toMonth>=9))){
                            if(s.Period_Calculation__c==15){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=9 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=7 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==15){
                                setKpiScores.add(s);
                            }
                        }
                        //Q4
                        if((fromYear==toYear) && ((fromMonth >=10 && fromMonth <=12) || (toMonth>=10 && toMonth<=12) || (fromMonth <=10 && toMonth==12))){
                            if(s.Period_Calculation__c==16){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=12 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=10 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==16){
                                setKpiScores.add(s);
                            }
                        }
                    }else if(kpi.Frequency__c=='Semi-Annually'){
                        //First Half
                        if((fromYear==toYear) && ((fromMonth >=1 && fromMonth <=6) || (toMonth>=1 && toMonth<=6))){
                            if(s.Period_Calculation__c==17){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=6 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=1 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==17){
                                setKpiScores.add(s);
                            }
                        }
                        //Second Half
                        if((fromYear==toYear) && ((fromMonth >=7 && fromMonth <=12) || (toMonth>=7 && toMonth<=12))){
                            if(s.Period_Calculation__c==18){
                                setKpiScores.add(s);
                            }
                        }else if((fromYear!=toYear) && ((fromMonth <=12 && s.Supplier_KPI__r.Evaluation_Period__c == fromYear) || (toMonth>=7 && s.Supplier_KPI__r.Evaluation_Period__c == toYear))){
                            if(s.Period_Calculation__c==18){
                                setKpiScores.add(s);
                            }
                        }
                    }
                    if(s.Period_Calculation__c==19){
                        setKpiScores.add(s);
                    }
                }
                listKpiScores.addAll(setKpiScores);
            }        
        }
        return listKpiScores;
    }
    
}