/**
*      @author Radhika Bahl
*      @date   12/11/2015
       @description  Test Class for JJ_SRM_SupplierKPIScorePopup

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Radhika Bahl                  12/11/2015         Created    
*/
@isTest(seeAllData=false)
private class JJ_SRM_SupplierKPIScorePopup_Test{
    private static User userRec;
    private static List<Account> Acclist =  new List<Account>();
    private static List<KPI_Master__c> MasterKPIlist =  new List<KPI_Master__c>();
    private static List<Supplier_KPI__c> SuppKPIlist =  new List<Supplier_KPI__c>();
    private static Score_Card__c card;
    private static String fromDate;
    private static String toDate;
    private static testMethod void JJ_SRM_SupplierKPIScorePopup_testMethod(){
        fromDate='1/8/2015';
        toDate='12/8/2016';
        PageReference pageRef = Page.JJ_SRM_SupplierKPIScorePopup;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.JJ_SRM_SupplierKPIScorePopup'));
        testSetup();
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj.finalListKpiScores).size(),2);
        fromDate='1/8/2017';
        toDate='12/8/2018';
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj1=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj1.finalListKpiScores).size(),2);
        fromDate='1/8/2016';
        toDate='1/8/2017';
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj2=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj2.finalListKpiScores).size(),2);
        fromDate='1/8/2015';
        toDate='12/8/2015';
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj3=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj3.finalListKpiScores).size(),1);
        fromDate='1/8/2016';
        toDate='12/8/2016';
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj4=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj4.finalListKpiScores).size(),1);
        fromDate='1/8/2017';
        toDate='12/8/2017';
        System.currentPageReference().getParameters().put('name', MasterKPIlist[0].name);
        System.currentPageReference().getParameters().put('FromDate',fromDate);
        System.currentPageReference().getParameters().put('ToDate',toDate);
        System.currentPageReference().getParameters().put('accountId',Acclist[0].Id);
        JJ_SRM_SupplierKPIScorePopup obj5=new JJ_SRM_SupplierKPIScorePopup();
        System.assertEquals((obj5.finalListKpiScores).size(),1);
        Test.stopTest();
    }
    
    static testMethod void testSetup() {
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(1,0);
        MasterKPIlist = JJ_SRM_TestDataSupplier.CreateMasterSuppKPI(50);
        List<KPI_Master__c> newList=new List<KPI_Master__c>();
        newList.add(MasterKPIlist[0]);
        SuppKPIlist = JJ_SRM_TestDataSupplier.CreateSuppKPISetup(50,newList,Acclist[0].Id);
    }
}