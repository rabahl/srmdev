/*****************************************************************************************************
Apex Class Name  : JJ_SRM_Task_EditCommentController
Version          : 1.0 
Created Date     : 10th March 2016
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------------------------
* Niranjan Unnamatla          03/10/2016             To modify the Comments field on Task 
******************************************************************************************************/
public class JJ_SRM_Task_EditCommentController{

    Public Task taskRecord {get; set;}
    Id taskId;
    
    public JJ_SRM_Task_EditCommentController(ApexPages.StandardController controller) {
        taskId = Controller.getRecord().id;
        taskRecord = [ Select Id, Description from Task where Id =: taskId ];
    }
    
    public void AddComments(){
        try{
            update taskRecord;
        }
        catch(exception ex){
            JJ_SRM_ExceptionUtility.singleExceptionLog('JJ_SRM_Task_EditCommentController', '', ex.getmessage() + ' and Line Number'+ ex.getLineNumber());
        }
    }
}