/**************************************************************************************************************************************************
Apex Class Name  : JJ_SRM_Task_Event_EditController_Test
Version          : 1.0 
Created Date     : 11th March 2016
                   
* Developer                   Date                   Description
* ------------------------------------------------------------------------------------------------------------------------------------------------
* Niranjan Unnamatla          03/11/2016             Test class for JJ_SRM_Task_EditCommentController and JJ_SRM_Event_OtherAgendaTopicsController and JJ_SRM_Reference_Materials_Controller
**************************************************************************************************************************************************/
@isTest
private class JJ_SRM_Task_Event_EditController_Test{
     private static testMethod void JJ_SRM_Task_Edit_Method(){
        Test.startTest();
        task taskrec = new task(description= 'testing');
        insert taskrec;
        ApexPages.StandardController taskcontroller =  new ApexPages.StandardController(taskrec);
        JJ_SRM_Task_EditCommentController te =  new JJ_SRM_Task_EditCommentController(taskcontroller);
        te.taskRecord.description= 'Description Modified';
        te.AddComments();
        taskrec = [select Description from task limit 1];
        system.assertequals(taskrec.description,'Description Modified');
        // Negative Testing by adding comments value more than allowed
        String maxDescriptionvalue = 'Description Modified';
        for(Integer i=0;i<15;i++){
            maxDescriptionvalue = maxDescriptionvalue + maxDescriptionvalue;
        }
        te.taskRecord.description = maxDescriptionvalue;
        te.AddComments();
        Test.stopTest();     
     }
     
     private static testMethod void JJ_SRM_Event_Edit_Method(){
        Test.startTest();        
        event eventrec = new event(Subject='Testing Agenda', StartDateTime=system.now(), enddatetime=system.now(),Agenda_Topics_Information__c='Testing');
        insert eventrec;
        JJ_SRM_Reference_Materials_Controller RefMaterial = New JJ_SRM_Reference_Materials_Controller();
        RefMaterial.RedirecttoKnowledge();
        ApexPages.StandardController eventcontroller =  new ApexPages.StandardController(eventrec);
        JJ_SRM_Event_OtherAgendaTopicsController eo =  new JJ_SRM_Event_OtherAgendaTopicsController(eventcontroller);
        eo.EventRecord.Agenda_Topics_Information__c='Other Agenda Topics modified';
        eo.AddOtherAgendaTopics();
        eventrec = [select Agenda_Topics_Information__c from event limit 1];
        system.assertequals(eventrec.Agenda_Topics_Information__c,'Other Agenda Topics modified');
        // Negative Testing by adding Agenda Topics value more than allowed
        String maxAgendavalue = 'Agenda Modified';
        for(Integer i=0;i<15;i++){
            maxAgendavalue = maxAgendavalue + maxAgendavalue;
        }
        eo.EventRecord.Agenda_Topics_Information__c = maxAgendavalue;
        eo.AddOtherAgendaTopics();
        Test.stopTest();
     }
}