/* Class Name   : JJ_SRM_TestDataSupplier
  * Description   : This class is used to create data for Supplier functionality
  * Created By   : Bhalodi Dhruv
  * Created Date : 15-Oct-2015
  * Modification Log: Modified by Bhalodi Dhruv on 15-Oct-2015
  * --------------------------------------------------------------------------------------------------------------------------------------
  * Developer                Date                 Modification ID        Description 
  * ---------------------------------------------------------------------------------------------------------------------------------------
  * Bhalodi Dhruv          15-Oct-2015                                 Initial Version

  */
@istest()
public  class JJ_SRM_TestDataSupplier{

 public static User createUser(String strUniqueKey, String strProfile){
        
        User oUSer                    = new User();
        oUser.Alias                   = strUniqueKey.left(4);
       // oUser.userroleid                = [Select Id from userrole where name =: 'Global ER/LR Director' LIMIT 1].id;
        oUser.Email                   = strUniqueKey + '@testjnj.org';
        oUser.EmailEncodingKey        = 'UTF-8';
        oUser.WWID_JNJ__c             = '123'+strUniqueKey.left(4);
        oUser.LastName                = 'User';
        oUser.FirstName               = strUniqueKey;
        oUser.LanguageLocaleKey       = 'en_US';
        oUser.LocaleSidKey            = 'en_US';
        oUser.ProfileId               = [Select Id From Profile Where Name =: strProfile Limit 1].Id;
        oUser.TimeZoneSidKey          = 'GMT';
        oUser.Username                = strUniqueKey + '@testjnj.org';
        oUser.Country                 = 'Belgium';
        return oUser; 
    }
    
    public static Score_Card__c createScoreCard(){
    
    Score_Card__c scr = new Score_Card__c();
    
    scr.Status__c = 'Published';
    scr.From__c = date.TODAY();
    scr.To__c = date.TODAY();
    //scr.Createdate = ;
    scr.Relationship_Summary__c = 'sdhf';
    scr.Summary_Quadrant1__c = 'dslkfn';
    scr.Summary_Quadrant2__c = 'qwerty';
    scr.Summary_Quadrant3__c = 'asdfdgh';
    scr.Summary_Quadrant4__c = 'zxcvb';
    scr.Potential_Total_Score__c = 'sdhfgdshf';
    scr.Overall_Score__c = 'ahdjah';
    scr.KPIs_Q1__c = 'a,b,z;';
    scr.KPIs_Q2__c = 'c,d,y;';
    scr.KPIs_Q3__c = 'e,f,x;';
    scr.KPIs_Q4__c = 'g,h,q;';
    scr.Facts__c = 'jdfdjhfdhfd';
    scr.RAG_Quadrant_1__c = 'R';
    scr.RAG_Quadrant_2__c = 'A';
    scr.RAG_Quadrant_3__c = 'G';
    scr.RAG_Quadrant_4__c = 'R';
    return scr;
}

     public static Contact createContact(String firstName, String lastName){
        
        // Set test field values
        Contact oContact = new Contact();
        oContact.FirstName = firstName;
        oContact.LastName = lastName;
        oContact.WWID__c = '123';
        oContact.Employee_Status__c = 'Inactive';
        //Added by shikha
        oContact.Email= firstname+'@test.com';
        oContact.Country_Name__c = 'USA';
        oContact.Phone = '0987654321';
       
        return oContact;
    }
    
    public static List<Governance__c> CreateGovernance(Integer Sequential, Integer NonSequential) {
        List<Governance__c> Govlist = new List<Governance__c>();
        for(Integer j=0;j<Sequential;j++){
            Governance__c gov = new Governance__c();
            gov.name = 'foo gov' + j;
            gov.Segment__c = 'S1'+ j;
            gov.required__c = 'Yes';
            gov.Suggested_Cadence__c = 4;
            Govlist.add(gov);
        }
        for(Integer j=0;j<NonSequential;j++){
            Governance__c gov = new Governance__c();
            gov.name = 'foo gov 1' + NonSequential;
            gov.Segment__c = 'S1';
            gov.required__c = 'Yes';
            gov.Suggested_Cadence__c = 4;
            Govlist.add(gov);
        }
        if(Govlist.size()>0)
            insert Govlist;
        return Govlist;
    }
    
   public static List<Account> CreateAccount(Integer Sequential, Integer NonSequential) {
        Id SuppRecordTypeId = JJ_SRM_StaticConstants.SupplierRecordTypeId;
        List<Account> Acclist = new List<Account>();
        for(Integer j=0;j<Sequential;j++){
            Account acc = new Account();
            acc.recordtypeId = SuppRecordTypeId;
            acc.name = 'foo gov' + j;
            acc.Segmentation__c = 'S1'+j;
            acc.Total_Spend_Current_Yr__c = 3;
            acc.Total_Spend_Previous_Yr__c = 3;
            acc.Active__c = True; 
            Acclist.add(acc);
        }
        for(Integer j=0;j<NonSequential;j++){
            Account acc = new Account();
            acc.recordtypeId = SuppRecordTypeId;
            acc.name = 'foo gov 1' + NonSequential;
            acc.Segmentation__c = 'S1';
            acc.Total_Spend_Current_Yr__c = 3;
            acc.Total_Spend_Previous_Yr__c = 3;
            acc.Active__c = True;
            Acclist.add(acc);
        }
        if(Acclist.size()>0)
            insert Acclist;
        return Acclist;
    }
    
     public static void Createtriggerstatus(List<String> Objectnames) {
        List<Trigger_Status__c> ObjstatusList = new List<Trigger_Status__c>();
        for(String s : Objectnames){
            Trigger_Status__c Triggerstatus = new Trigger_Status__c();
            Triggerstatus.name = s;
            Triggerstatus.Active__c = true;
            ObjstatusList.add(Triggerstatus);
        }
        if(ObjstatusList.size()>0)
            insert ObjstatusList;
    }
    
    public static void Createrolesetting(List<String> Objectnames) {
        /*List<JJ_SRM_Contact_Role_Mappings__c> ObjroleList = new List<JJ_SRM_Contact_Role_Mappings__c>();
       
        for(String objectsetting : Objectnames){   
            JJ_SRM_Contact_Role_Mappings__c custsetting = new JJ_SRM_Contact_Role_Mappings__c();
            custsetting.name = objectsetting;
            custsetting.Responsibilities__c='Input on financial matters ' + objectsetting;
            ObjroleList.add(custsetting);
        }
        Insert ObjroleList;*/
    }
    
    public static List<KPI_Master__c> CreateMasterSuppKPI(Integer i){
        List<KPI_Master__c> MastSuppKpilist = new List<KPI_Master__c>();        
        for(Integer j=0;j<i;j++){
            KPI_Master__c TempKpi = new KPI_Master__c();
            TempKpi.name='TestKPI_'+j;
            TempKpi.Unit__c = 'Response';
            TempKpi.Other_Value_Red__c = 'Yes';
            TempKpi.Other_Value_Green__c = 'No';
            TempKpi.Calculations__c='testcalculations';
            TempKpi.Definition__c='testdefinition';
            if(j>0){
                if(math.mod(j,4) == 0)
                    TempKpi.KPI_Library_Level_1__c = 'Account Management';
                else if(math.mod(j,4) == 1)
                    TempKpi.KPI_Library_Level_1__c = 'Innovation';
                else if(math.mod(j,4) == 2)
                    TempKpi.KPI_Library_Level_1__c = 'Performance';
                else if(math.mod(j,4) == 3)
                    TempKpi.KPI_Library_Level_1__c = 'Risk & Compliance'; 
            }
            MastSuppKpilist.add(TempKpi);     
        }
        if(!(MastSuppKpilist.isempty())){
            Insert MastSuppKpilist;
        }
        return MastSuppKpilist;
    }
    
    public static List<Supplier_KPI__c> CreateSuppKPI(Integer i,List<KPI_Master__c> MastKPi, Id AccountId){
        List<Supplier_KPI__c> SuppKpilist = new List<Supplier_KPI__c>();        
        for(Integer j=0;j<i;j++){
            Supplier_KPI__c TempKpi = new Supplier_KPI__c();
            TempKpi.Supplier_Name__c = AccountId;
            TempKpi.Evaluation_Period__c = '20'+j+j;
            if(j>0 && j<20){
                if(math.mod(j,2) == 1)
                    TempKpi.Color_Status__c = 'Red';
            }
            if(j>20){
                if(math.mod(j,2) == 1)
                    TempKpi.Color_Status__c = '';
                else
                    TempKpi.Color_Status__c = 'Red';
            }
            if(MastKPi.size()>j && MastKPi[j]!=null){
                TempKpi.KPI_Master__c = MastKPi[j].Id;
                TempKpi.Frequency__c='Monthly';  
                TempKpi.Unit__c='Response';            
                SuppKpilist.add(TempKpi); 
               
            }         
        }
        if(!(SuppKpilist.isempty())){
          
            Insert SuppKpilist;
        }
        return SuppKpilist;
    }
    public static List<Supplier_KPI_Score__c> CreateSuppKPIScores(List<Supplier_KPI__c> SuppKPIList){
        List<Supplier_KPI_Score__c> scoreList=new List<Supplier_KPI_Score__c>();
        Supplier_KPI_Score__c tempScore;
        for(Supplier_KPI__c sk:SuppKPIList){
       
            if(sk.Frequency__c!=null && sk.Unit__c!=null){
                tempScore=new Supplier_KPI_Score__c();
                tempScore.Supplier_KPI__c=sk.Id;
                tempScore.Frequency__c=sk.Frequency__c;
                if(tempScore.Frequency__c=='Monthly'){
                    tempScore.Period__c='January';
                }else if(tempScore.Frequency__c=='Quarterly'){
                    tempScore.Period__c='Q1';
                }else if(tempScore.Frequency__c=='Semi-Annually'){
                    tempScore.Period__c='First Half';
                }else{
                    tempScore.Period__c='Yearly';
                }
                tempScore.Unit__c=sk.unit__c;
                tempScore.Score__c='Yes'; 
                scoreList.add(tempScore);
            }
        }
        if(!scoreList.isEmpty()){
            insert scoreList;
        }
        
        return scoreList;
    }

    public static List<Action_Item_Log__c> CreateTrackingLog(Integer Sequential ){
         List<Action_Item_Log__c> TL = new List<Action_Item_Log__c>();
            for(Integer j=0;j<Sequential;j++){
                Action_Item_Log__c tlc = new Action_Item_Log__c();
                tlc.name = 'Trackinglog' + j;
                TL.add(tlc);
                }
                insert TL;
                return TL;
    }
    
    public static List<Supplier_KPI__c> CreateSuppKPISetup(Integer i,List<KPI_Master__c> MastKPi, Id AccountId){
        List<Supplier_KPI__c> SuppKpilist = new List<Supplier_KPI__c>();        
        for(Integer j=0;j<i;j++){
            Supplier_KPI__c TempKpi = new Supplier_KPI__c();
            Supplier_KPI__c TempKpi1 = new Supplier_KPI__c();
            Supplier_KPI__c TempKpi2 = new Supplier_KPI__c();
            Supplier_KPI__c TempKpi3 = new Supplier_KPI__c();
            TempKpi.Supplier_Name__c = AccountId;
            TempKpi.Evaluation_Period__c = '2015';
            TempKpi1.Supplier_Name__c = AccountId;
            TempKpi1.Evaluation_Period__c = '2016';
            TempKpi2.Supplier_Name__c = AccountId;
            TempKpi2.Evaluation_Period__c = '2017';
            TempKpi3.Supplier_Name__c = AccountId;
            TempKpi3.Evaluation_Period__c = '2018';
            if(j>0 && j<20){
                if(math.mod(j,2) == 1)
                    TempKpi.Color_Status__c = 'Red';
            }
            if(j>20){
                if(math.mod(j,2) == 1)
                    TempKpi.Color_Status__c = '';
                else
                    TempKpi.Color_Status__c = 'Red';
            }
            if(MastKPi.size()>j && MastKPi[j]!=null){
                TempKpi.KPI_Master__c = MastKPi[j].Id;
                TempKpi.Frequency__c='Monthly';  
                TempKpi.Unit__c='Response';            
                SuppKpilist.add(TempKpi); 
                TempKpi1.KPI_Master__c = MastKPi[j].Id;
                TempKpi1.Frequency__c='Quarterly';  
                TempKpi1.Unit__c='Response';            
                SuppKpilist.add(TempKpi1); 
                TempKpi2.KPI_Master__c = MastKPi[j].Id;
                TempKpi2.Frequency__c='Semi-Annually';  
                TempKpi2.Unit__c='Response';            
                SuppKpilist.add(TempKpi2); 
                TempKpi3.KPI_Master__c = MastKPi[j].Id;
                TempKpi3.Frequency__c='Annually';  
                TempKpi3.Unit__c='Response';            
                SuppKpilist.add(TempKpi3); 
                
            }         
        }
        if(!(SuppKpilist.isempty())){
            
            Insert SuppKpilist;
            CreateSuppKPIScores(SuppKpilist);
        }
        return SuppKpilist;
    }
    
  
}