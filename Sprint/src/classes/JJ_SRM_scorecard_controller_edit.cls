/**************************************************************************************
Apex Class Name  : JJ_SRM_scorecard_controller_edit
Version          : 1.0 
Created Date     : 17th November 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte            14th November 2015        Original Version
* Radhika Bahl        07th December 2015        Added logic for US-5215 and US-5053. Also added method findLatestPeriodScore().
*************************************************************************************/
public with sharing class JJ_SRM_scorecard_controller_edit{
    
    public Score_Card__c card {get;set;}
    public Id ScoreCardId {get;set;}
    public Id AccountId {get;set;}
    public Account Supplier {get; set;}
    public Boolean Published {get; set;}
    public Boolean DisplaySelectAllQ1 {get; set;}
    public Boolean DisplaySelectAllQ2 {get; set;}
    public Boolean DisplaySelectAllQ3 {get; set;}
    public Boolean DisplaySelectAllQ4 {get; set;}
    public List<Quadrantdetails> Quadrantdetailwrapper{get; set;}
    public Map<String,List<Quadrantdetails>> mapQuadrant{get; set;}
    public String FromdateError{get; set;}
    public String TodateError{get; set;}
    public String TitleError{get;set;}
    public Integer Q1Hidden{get; set;}
    public Set<Id> setSKPI;
    public List<Supplier_KPI_Score__c> listScores;
    public List<Supplier_KPI_Score__c> listUpdateScores;
    public Set<Supplier_KPI_Score__c> setKpiScores;
    public List<Supplier_KPI_Score__c> listKpiScores;
    public Integer fromMonth;
    public Integer toMonth;
    public String fromYear;
    public String toYear;
    public List<Supplier_KPI_Score__c> listOfUpdatedScores;
    public Map<Id,List<Supplier_KPI_Score__c>> mapKPIAndListUpdatedScores;
    public List<Supplier_KPI__c> listSKPI;
    public List<Supplier_KPI__c> listUpdatedKPI;
    public Set<Id> setScoreIds;
    Database.saveResult sr;
    Supplier_KPI_Score__c latestScore;
    
    /*
    * @description: This constructor is used to load values on screen
    * @param: None
    * @return Type: None
    * Constructor type
    */ 
    
    public JJ_SRM_scorecard_controller_edit(ApexPages.StandardController controller){
        card = (Score_Card__c)controller.getRecord();
        DisplaySelectAllQ1 = True; 
        DisplaySelectAllQ2 = True;
        DisplaySelectAllQ3 = True;
        DisplaySelectAllQ4 = True;
        Quadrantdetailwrapper = new List<Quadrantdetails>();
        mapQuadrant = new Map<String,List<Quadrantdetails>>();
        String AccountFields;
        String AccQuery;
        Published = False;
        ScoreCardId = card.Id;
        Q1Hidden = 0;
        List<String> tempKPI;
        Map<Id,Supplier_KPI__c> SuppKpimap = new Map<Id,Supplier_KPI__c>();
        Set<String> ExistingKPIs = new Set<String>();
        Set<String> ComparisionofExistingKPIs = new Set<String>();
        List<Supplier_KPI__c> supplierKPIs;
        try{       
            if(ScoreCardId != null){
                card = [Select id, name, Status__c, Overall_RAG__c, From__c, To__c, Createddate, Relationship_Summary__c, Summary_Quadrant1__c, Summary_Quadrant2__c, Summary_Quadrant3__c,
                                Summary_Quadrant4__c, Potential_Total_Score__c, KPIs_Q1__c, KPIs_Q2__c, KPIs_Q3__c,
                                KPIs_Q4__c, Facts__c, RAG_Quadrant_1__c, RAG_Quadrant_2__c, RAG_Quadrant_3__c, RAG_Quadrant_4__c, Supplier__c from Score_Card__c where Id = : ScoreCardId];
                if(card.KPIs_Q1__c != null)
                    ExistingKPIs.addall(card.KPIs_Q1__c.split(';'));
                if(card.KPIs_Q2__c != null)
                    ExistingKPIs.addall(card.KPIs_Q2__c.split(';'));
                if(card.KPIs_Q3__c != null)
                    ExistingKPIs.addall(card.KPIs_Q3__c.split(';'));
                if(card.KPIs_Q4__c != null)
                    ExistingKPIs.addall(card.KPIs_Q4__c.split(';'));
            }
            if(card.Status__c == 'Published')
                Published = True;
            if(card.Status__c == '' || card.Status__c == null)
                card.Status__c = 'Draft';
            if(ScoreCardId != null)
                AccountId = card.Supplier__c;
            else
                AccountId = ApexPages.currentPage().getParameters().get(Label.ScoreCardAccountField);
            Supplier = [Select id, Name, Primary_Category__c, Owner.Email, Supplier_Key_Contact__r.name, Owner.Name, Supplier_Key_Contact__r.Email  from Account where Id = : AccountId];
            supplierKPIs = [Select id, KPI_Master__r.KPI_Library_Level_1__c,Evaluation_Period__c, Color_Status__c, KPI_Master__r.Name, Name  from Supplier_KPI__c Where Supplier_Name__c = :AccountId];
            for(Supplier_KPI__c SuppKpi : supplierKPIs){
                SuppKpimap.put(SuppKpi.Id,SuppKpi); 
            }
            for(String KPIQ : ExistingKPIs){
                tempKPI = new List<String>();
                tempKPI.addall(KPIQ.split(','));
                ComparisionofExistingKPIs.add(tempKPI[1]);
                if(SuppKpimap.containskey(tempKPI[0])){
                    Quadrantdetails TempWrapper = new Quadrantdetails();
                    TempWrapper.RAGscore.add(new SelectOption('',''));
                    TempWrapper.RAGscore.add(new SelectOption('Red','Red'));
                    TempWrapper.RAGscore.add(new SelectOption('Amber','Amber'));
                    TempWrapper.RAGscore.add(new SelectOption('Green','Green'));
                    TempWrapper.selectedRAG = tempKPI[2];
                    TempWrapper.MasterName = SuppKpimap.get(tempKPI[0]).KPI_Master__r.Name;
                    TempWrapper.EncodedMasterName = EncodingUtil.urlEncode(SuppKpimap.get(tempKPI[0]).KPI_Master__r.Name,'UTF-8');
                    TempWrapper.SuppKPI = tempKPI[0];
                    TempWrapper.Quadrant = SuppKpimap.get(tempKPI[0]).KPI_Master__r.KPI_Library_Level_1__c;
                    TempWrapper.SelectedKPI = True;
                    Quadrantdetailwrapper.add(TempWrapper);
                }
            }
            for(Supplier_KPI__c suppKPI : supplierKPIs){
                if(!((ComparisionofExistingKPIs.contains(suppKPI.KPI_Master__r.Name)))){
                    ComparisionofExistingKPIs.add(suppKPI.KPI_Master__r.Name);
                    Quadrantdetails TempWrapper = new Quadrantdetails();
                    TempWrapper.RAGscore.add(new SelectOption('',''));
                    TempWrapper.RAGscore.add(new SelectOption('Red','Red'));
                    TempWrapper.RAGscore.add(new SelectOption('Amber','Amber'));
                    TempWrapper.RAGscore.add(new SelectOption('Green','Green'));
                    TempWrapper.MasterName = suppKPI.KPI_Master__r.Name;
                    TempWrapper.EncodedMasterName = EncodingUtil.urlEncode(suppKPI.KPI_Master__r.Name,'UTF-8');
                    TempWrapper.SuppKPI = suppKPI.Id;
                    TempWrapper.Quadrant = suppKPI.KPI_Master__r.KPI_Library_Level_1__c;
                    TempWrapper.SelectedKPI = False;
                    Quadrantdetailwrapper.add(TempWrapper);
                }
            }
            // putting blank values in map so as avoid null pointer error on page level
            List<Quadrantdetails> tempListnull = new List<Quadrantdetails>();
            mapQuadrant.put(Label.ScoreCardQuadrant1,tempListnull);
            mapQuadrant.put(Label.ScoreCardQuadrant2,tempListnull);
            mapQuadrant.put(Label.ScoreCardQuadrant3,tempListnull);
            mapQuadrant.put(Label.ScoreCardQuadrant4,tempListnull);
            for(Quadrantdetails Quadwrapper : Quadrantdetailwrapper){
                if(mapQuadrant.containskey(Quadwrapper.Quadrant)){
                    List<Quadrantdetails> tempList = new List<Quadrantdetails>();
                    tempList.addall(mapQuadrant.remove(Quadwrapper.Quadrant));
                    tempList.add(Quadwrapper);
                    mapQuadrant.put(Quadwrapper.Quadrant,tempList);
                }
                else{
                    List<Quadrantdetails> tempList = new List<Quadrantdetails>();
                    tempList.add(Quadwrapper);
                    mapQuadrant.put(Quadwrapper.Quadrant,tempList);
                }
            }  
            if(mapQuadrant.containskey(Label.ScoreCardQuadrant1) && mapQuadrant.get(Label.ScoreCardQuadrant1).size() > Integer.valueof(Label.JJ_SRM_MultiSelectLimit))
                DisplaySelectAllQ1 = False; 
            if(mapQuadrant.containskey(Label.ScoreCardQuadrant2) && mapQuadrant.get(Label.ScoreCardQuadrant2).size() > Integer.valueof(Label.JJ_SRM_MultiSelectLimit))
                DisplaySelectAllQ2 = False; 
            if(mapQuadrant.containskey(Label.ScoreCardQuadrant3) && mapQuadrant.get(Label.ScoreCardQuadrant3).size() > Integer.valueof(Label.JJ_SRM_MultiSelectLimit))
                DisplaySelectAllQ3 = False;
            if(mapQuadrant.containskey(Label.ScoreCardQuadrant4) && mapQuadrant.get(Label.ScoreCardQuadrant4).size() > Integer.valueof(Label.JJ_SRM_MultiSelectLimit))
                DisplaySelectAllQ4 = False;        
        }
        catch(exception e){
            //Cannot log an exception in constructor
        }
    }
  
    /*
    * @description: This  method is used to save data on scorecard
    * @param: None
    * @return Type: None
    */  
    public pagereference saveScoreCard(){
        card.KPIs_Q1__c = '';
        card.KPIs_Q2__c = '';
        card.KPIs_Q3__c = '';
        card.KPIs_Q4__c = '';
        FromdateError = '';
        TodateError = '';
        TitleError = '';
        setSKPI=new Set<Id>();
        try{
            for (Quadrantdetails TempQuad : Quadrantdetailwrapper){
                if(TempQuad.Quadrant == Label.ScoreCardQuadrant1 && TempQuad.SelectedKPI == True){                  
                    card.KPIs_Q1__c = card.KPIs_Q1__c + TempQuad.SuppKPI + ',' + TempQuad.MasterName + ',' + TempQuad.selectedRAG + ';'; 
                }
                if(TempQuad.Quadrant == Label.ScoreCardQuadrant2 && TempQuad.SelectedKPI == True){
                    card.KPIs_Q2__c = card.KPIs_Q2__c + TempQuad.SuppKPI + ',' + TempQuad.MasterName + ',' + TempQuad.selectedRAG + ';';  
                }
                if(TempQuad.Quadrant == Label.ScoreCardQuadrant3 && TempQuad.SelectedKPI == True){
                    card.KPIs_Q3__c = card.KPIs_Q3__c + TempQuad.SuppKPI + ',' + TempQuad.MasterName + ',' + TempQuad.selectedRAG + ';';  
                }
                if(TempQuad.Quadrant == Label.ScoreCardQuadrant4 && TempQuad.SelectedKPI == True){
                    card.KPIs_Q4__c = card.KPIs_Q4__c + TempQuad.SuppKPI + ',' + TempQuad.MasterName + ',' + TempQuad.selectedRAG + ';';  
                }
            }
            if(card.From__c == null || card.To__c == null || card.Name == null){
                if(card.From__c == null)
                    FromdateError = Label.From_Date_Error_Message_Scorecard;
                if(card.To__c == null)
                    TodateError = Label.To_Date_Error_Message_Scorecard;
                if(card.name == null)
                    TitleError = Label.ScoreCard_Title_Error_Message;
                return null;
            }
            if(card.From__c > card.To__c){
                FromdateError = Label.From_Date_Greater_Than_To_Date_Error_Scorecard;  
                return null;
            }
            
            // Logic added to Share published scorecards with external Supplier
            
            if(card.Status__c == 'Published')
                card.Share_With_Supplier__c = AccountId;
                
            // Logic added to Share published scorecards with external Supplier
            if(ScoreCardId != null)
                sr=Database.update(card,false);
            if(ScoreCardId == null)
                sr=Database.insert(card,false);
            
            //Begin:: US-5215 and US-5053
            if(card.From__c!=null){
                fromMonth=card.From__c.month();
                fromYear=String.valueOf(card.From__c.Year());
            }
            if(card.To__c!=null){
                toMonth=card.To__c.month();
                toYear=String.valueOf(card.To__c.Year());
            }
            listScores=new List<Supplier_KPI_Score__c>();
            listUpdateScores=new List<Supplier_KPI_Score__c>();
            listKpiScores=new List<Supplier_KPI_Score__c>();
            setKpiScores=new Set<Supplier_KPI_Score__c>();
            JJ_SRM_SupplierKPIScorePopup obj=new JJ_SRM_SupplierKPIScorePopup();
            if(sr.isSuccess() && card.Status__c.equalsIgnoreCase('Published')){
                //insert or update is successful, now lock selected KPI scores
                for(List<Quadrantdetails> listQD:mapQuadrant.values()){
                    for(Quadrantdetails qd:listQD){
                        if(qd.SelectedKPI==true){
                            //creating set of all selected Supplier KPIs
                            setSKPI.add(qd.SuppKPI);
                            //calling method - to process KPIs and Scores based upon From and To dates
                            listKpiScores.addall(obj.processKPIAndScores(qd.MasterName,AccountId,fromYear,toYear, fromMonth,toMonth));
                        }
                    }
                }
            }
            /*
            //if selected KPI exist on the UI then query all underlying Supplier KPI Scores
            if(!setSKPI.isEmpty()){
                listScores=[select id,Score_Card__c,Locked__c,Supplier_KPI__c,name,RAG__c,Score__c,Period__c,Period_Calculation__c from Supplier_KPI_Score__c where Supplier_KPI__c IN:setSKPI];
                if(!listScores.isEmpty()){
                    for(Supplier_KPI_Score__c score:listScores){
                        //filter out Scores based on from and to date
                        if(score.Period_Calculation__c == fromMonth || score.Period_Calculation__c == toMonth){
                            setKpiScores.add(score);
                        }
                        if(score.Period_Calculation__c==19){
                            setKpiScores.add(score);
                        }
                        if(score.Period_Calculation__c>=fromMonth && score.Period_Calculation__c<=toMonth){
                            setKpiScores.add(score);
                        }
                        //Q1
                        if((fromMonth >=1 && fromMonth <=3) || (toMonth>=1 && toMonth<=3)){
                            if(score.Period_Calculation__c==13){
                                setKpiScores.add(score);
                            }
                        }
                        //Q2
                        if((fromMonth >=4 && fromMonth <=6) || (toMonth>=4 && toMonth<=6) || (fromMonth <=4 && toMonth>=6)){
                            if(score.Period_Calculation__c==14){
                                setKpiScores.add(score);
                            }
                        }
                        //Q3
                        if((fromMonth >=7 && fromMonth <=9) || (toMonth>=7 && toMonth<=9) || (fromMonth <=7 && toMonth>=9)){
                            if(score.Period_Calculation__c==15){
                                setKpiScores.add(score);
                            }
                        }
                        //Q4
                        if((fromMonth >=10 && fromMonth <=12) || (toMonth>=10 && toMonth<=12) || (fromMonth <=10 && toMonth==12)){
                            if(score.Period_Calculation__c==16){
                                setKpiScores.add(score);
                            }
                        }
                        //First Half
                        if((fromMonth >=1 && fromMonth <=6) || (toMonth>=1 && toMonth<=6)){
                            if(score.Period_Calculation__c==17){
                                setKpiScores.add(score);
                            }
                        }
                        //Second Half
                        if((fromMonth >=7 && fromMonth <=12) || (toMonth>=7 && toMonth<=12)){
                            if(score.Period_Calculation__c==18){
                                setKpiScores.add(score);
                            }
                        }
                    }
                    listKpiScores.addAll(setKpiScores);
                    
                }*/
                if(!listKpiScores.isEmpty()){
                    for(Supplier_KPI_Score__c sc:listKpiScores){
                        sc.Locked__c=true;
                        if(card.Id!=null){
                            sc.Score_Card__c=card.Id;
                        }
                        listUpdateScores.add(sc);
                    }
                }
                List<DataBase.saveResult> srList=new List<DataBase.saveResult>();
                //update scores in database
                if(!listUpdateScores.isEmpty()){
                    srList=DataBase.update(listUpdateScores,false);
                }
                
                //Begin:: After locking the KPI scores, iterate over locked scores and determine the latest period one. RAG and Score of this would be used to update its parent KPI.
                
                //list of successfully updated KPI scores
                setScoreIds=new Set<Id>();
                listOfUpdatedScores=new List<Supplier_KPI_Score__c>();
                //map of Supplier KPI and successfully updated KPI scores under it
                mapKPIAndListUpdatedScores = new Map<Id,List<Supplier_KPI_Score__c>>();
                
                for(DataBase.saveResult sr:srList){
                    if(sr.isSuccess()){
                        setScoreIds.add(sr.getId());
                    }
                }
                if(!setScoreIds.isEmpty()){
                    listOfUpdatedScores=[select Supplier_KPI__c,name,RAG__c,RAG_Text_Only__c,Score__c,Period__c,Period_Calculation__c from Supplier_KPI_Score__c where ID IN:setScoreIds];
                }
                List<Supplier_KPI_Score__c> tempList;
                for(Supplier_KPI_Score__c score:listOfUpdatedScores){
                    if(mapKPIAndListUpdatedScores.containsKey(score.Supplier_KPI__c)){
                        tempList=new List<Supplier_KPI_Score__c>();
                        tempList= mapKPIAndListUpdatedScores.get(score.Supplier_KPI__c);
                        tempList.add(score);
                        mapKPIAndListUpdatedScores.put(score.Supplier_KPI__c,tempList);
                    }else{
                        tempList=new List<Supplier_KPI_Score__c>();
                        tempList.add(score);
                        mapKPIAndListUpdatedScores.put(score.Supplier_KPI__c,tempList);
                    }
                }
                listSKPI=new List<Supplier_KPI__c>();
                //fetching Supplier KPIs to be updated with RAG and Score
                if(!mapKPIAndListUpdatedScores.isEmpty()){
                    listSKPI=[select id, Color_Status__c, Frequency__c,Latest_Locked_Score__c, KPI_Score__c from Supplier_KPI__c where Id IN:mapKPIAndListUpdatedScores.keySet()];
                }
                listUpdatedKPI=new List<Supplier_KPI__c>();
                for(Supplier_KPI__c skpi:listSKPI){
                    //calling method findLatestPeriodScore - to find latest Period-Locked score under a KPI
                    latestScore=findLatestPeriodScore(mapKPIAndListUpdatedScores.get(skpi.id),Integer.valueOf(skpi.Latest_Locked_Score__c));
                    skpi.Color_Status__c=latestScore.RAG_Text_Only__c;
                    skpi.KPI_Score__c=latestScore.Score__c;
                    listUpdatedKPI.add(skpi);
                }
                if(!listUpdatedKPI.isEmpty()){
                    List<DataBase.saveResult> srList1=DataBase.update(listUpdatedKPI,false);
                }
                //End
                //End:: US-5215 and US-5053
            }
        catch(exception ee){
            JJ_SRM_ExceptionUtility.singleExceptionLog('JJ_SRM_scorecard_controller_edit', '', ee.getmessage()+' and Line Number'+ee.getLineNumber());
        }
        PageReference pageRef = Page.JJ_SRM_ScoreCard_View;
        pageRef.setRedirect(true);
        pageRef.getParameters().put('id',card.Id);
        return pageRef;   
    }
    
    /*
    * @description: This  method is used to find score with most recent Period value
    * @param: List<Supplier_KPI_Score__c>,Integer
    * @return Type: Supplier_KPI_Score__c
    */ 
    public Supplier_KPI_Score__c findLatestPeriodScore(List<Supplier_KPI_Score__c> listOfScores,Integer period){
        for(Supplier_KPI_Score__c score:listOfScores){
            if(score.Period_Calculation__c==period){
                return score;
            }
        }
        return null;
    }
    
    /*
    * @description: This  method is used to redirect user to view screen if published
    * @param: None
    * @return Type: None
    */  
    public pagereference RedirectView(){
        PageReference pageRef;
        if(Published){  
            pageRef = Page.JJ_SRM_ScoreCard_View;    
            pageRef.setRedirect(true);
            pageRef.getParameters().put('id',card.Id);           
        }  
    SavePoint sp = DataBase.setSavepoint();
     // check if the user has read write access to Scorecard or else give error
    try{
    UserRecordAccess urs = [select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :ApexPages.currentPage().getParameters().get('Id')];
    if (!(urs.HasEditAccess)){
        pageRef = Page.JJ_SRM_Error_Page;    
        pageRef.setRedirect(true);       
    }
    }
    catch(exception e){
    
    }
  
    // check if the user has read write access to account or else give error
    try{       
    UserRecordAccess ura = [select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :ApexPages.currentPage().getParameters().get(Label.ScoreCardAccountField)];
    if (!(ura.HasEditAccess)){
        pageRef = Page.JJ_SRM_Error_Page;    
        pageRef.setRedirect(true);       
    }
    }
    catch(exception e){
    
    }
    // Rollback, b/c the update was only to validate if the user can update the record
        Database.rollback(sp);
        return pageRef; 
    }
    
    
    //wrapper class for holding Supp KPI info in Quadrant
    public class Quadrantdetails
    {
        public List<SelectOption> RAGscore = new List<SelectOption>();
        public String selectedRAG {get; set;}
        public Id SuppKPI {get;set;}
        public String MasterName {get;set;}
        public String Quadrant {get;set;}
        public Boolean SelectedKPI {get; set;}
        public String EncodedMasterName {get;set;}
          
        public Quadrantdetails()
        {
        }
          
        public List<SelectOption> getRAGscore() {
            return RAGscore; 
        }
    }
}