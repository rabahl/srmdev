/**************************************************************************************
Apex Class Name  : JJ_SRM_scorecard_controller_edit_Test
Version          : 1.0
Created Date     : 23rd November 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Puneet Gupta        23rd November 2015        Original Version
*************************************************************************************/
@isTest(seeAllData=false)
private class JJ_SRM_scorecard_controller_edit_Test{
    private static User userRec;
    private static List<Account> Acclist =  new List<Account>();
    private static List<KPI_Master__c> MasterKPIlist =  new List<KPI_Master__c>();
    private static List<Supplier_KPI__c> SuppKPIlist =  new List<Supplier_KPI__c>();
    private static List<Supplier_KPI_Score__c> SuppKPIScorelist =  new List<Supplier_KPI_Score__c>();
    private static Score_Card__c card;
    static testMethod void testSetup() {
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(1,0);
        MasterKPIlist = JJ_SRM_TestDataSupplier.CreateMasterSuppKPI(10);
        SuppKPIlist = JJ_SRM_TestDataSupplier.CreateSuppKPI(10,MasterKPIlist,Acclist[0].Id);
        SuppKPIScorelist = JJ_SRM_TestDataSupplier.CreateSuppKPIScores(SuppKPIlist);
        card= JJ_SRM_TestDataSupplier.createScoreCard();
        card.Supplier__c=Acclist[0].Id;
        card.KPIs_Q1__c = 'R,A,G';
        card.KPIs_Q2__c = 'R,A,G';
        card.KPIs_Q3__c = 'R,A,G';
        card.KPIs_Q4__c = 'R,A,G';
        card.Summary_Quadrant1__c='Summary1';
        card.Summary_Quadrant2__c='Summary2';
        card.Summary_Quadrant3__c='Summary3';
        card.Summary_Quadrant4__c='Summary4';
        if(card!=null){
            insert card;
        }
    }
    static testMethod void getScoreCardTest() {
        Test.startTest();
        testSetup();
        ApexPages.StandardController sc = new ApexPages.StandardController(card);
        JJ_SRM_scorecard_controller_edit editpage = new JJ_SRM_scorecard_controller_edit(sc);
        editpage.saveScoreCard();
        card = [select id,KPIs_Q1__c,KPIs_Q2__c,KPIs_Q3__c,KPIs_Q4__c from Score_Card__c where Id =: card.id];
        card.KPIs_Q1__c = SuppKPIlist[0].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[4].Id + ',KPIMaster1,' + 'G;';
        card.KPIs_Q2__c = SuppKPIlist[1].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[5].Id + ',KPIMaster1,' + 'G;';
        card.KPIs_Q3__c = SuppKPIlist[2].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[6].Id + ',KPIMaster1,' + 'G;';
        card.KPIs_Q4__c = SuppKPIlist[3].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[7].Id + ',KPIMaster1,' + 'G;';
        
        card.Summary_Quadrant1__c='Summary11';
        card.Summary_Quadrant2__c='Summary21';
        card.Summary_Quadrant3__c='Summary31';
        card.Summary_Quadrant4__c='Summary41';
        if(card!=null){
            Update card;
        }
        ApexPages.StandardController sc2 = new ApexPages.StandardController(card);
        JJ_SRM_scorecard_controller_edit editpage2 = new JJ_SRM_scorecard_controller_edit(sc2);      
        editpage2.saveScoreCard();
        editpage2.RedirectView();
        ApexPages.StandardController sc3 = new ApexPages.StandardController(card);
        JJ_SRM_scorecard_controller_edit editpage3 = new JJ_SRM_scorecard_controller_edit(sc3);   
        editpage3.card.From__c = null;  
        editpage3.card.To__c = null;  
        editpage3.card.Name = null;   
        editpage3.saveScoreCard();
        editpage3.card.From__c = date.parse('01/01/2011');
        editpage3.card.To__c = date.parse('02/02/2012');
        editpage3.card.Name = 'Test Scorecard';
        editpage3.saveScoreCard();
        system.assertequals(card.KPIs_Q1__c,SuppKPIlist[0].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[4].Id + ',KPIMaster1,' + 'G;');
        system.assertequals(card.KPIs_Q2__c,SuppKPIlist[1].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[5].Id + ',KPIMaster1,' + 'G;');
        system.assertequals(card.KPIs_Q3__c,SuppKPIlist[2].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[6].Id + ',KPIMaster1,' + 'G;');
        system.assertequals(card.KPIs_Q4__c,SuppKPIlist[3].Id + ',KPIMaster1,' + 'G;' + SuppKPIlist[7].Id + ',KPIMaster1,' + 'G;');
        system.assertequals(card.Summary_Quadrant1__c,'Summary11');
        system.assertequals(card.Summary_Quadrant2__c,'Summary21');
        system.assertequals(card.Summary_Quadrant3__c,'Summary31');
        system.assertequals(card.Summary_Quadrant4__c,'Summary41');
        Test.stopTest();
    }
    
}