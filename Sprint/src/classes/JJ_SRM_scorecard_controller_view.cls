/**************************************************************************************
Apex Class Name  : JJ_SRM_scorecard_controller_view
Version          : 1.0
Created Date     : 17th November 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte            14th November 2015        Original Version
*************************************************************************************/
public with sharing class JJ_SRM_scorecard_controller_view{

    public JJ_SRM_scorecard_controller_view() {
        // Nothing to load in controller for the page
    }

    
    public Score_Card__c card {get;set;}
    public Id ScoreCardId {get;set;}
    public Id AccountId {get;set;}
    public Account Supplier {get; set;}
    public Boolean Published {get; set;}
    public List<String> selectedKPIsQ1 { get; set; }
    public List<String> selectedKPIsQ2 { get; set; }
    public List<String> selectedKPIsQ3 { get; set; }
    public List<String> selectedKPIsQ4 { get; set; }
    
      /*
    * @description: This constructor is used to load values on screen
    * @param: None
    * @return Type: None
    */ 
    
    public JJ_SRM_scorecard_controller_view(ApexPages.StandardController controller){
        // get the associated score card with the page
        card = (Score_Card__c)controller.getRecord();
        ScoreCardId = card.Id;
    }
    
    /*
    * @description: This  method is used to get data to be displayed on the VF page.
    * @param: None
    * @return Type: None
    */  
    public void getScoreCard(){
    String AccountFields;
    String AccQuery;
    Published = False;
    List<String> tempKPI;
    selectedKPIsQ1 = new List<String>();
    selectedKPIsQ2 = new List<String>();
    selectedKPIsQ3 = new List<String>();
    selectedKPIsQ4 = new List<String>();
    Set<String> ExistingKPIQ1 = new Set<String>();
    Set<String> ExistingKPIQ2 = new Set<String>();
    Set<String> ExistingKPIQ3 = new Set<String>();
    Set<String> ExistingKPIQ4 = new Set<String>();
    try{
        if(ScoreCardId != null){
            card = [Select id, name, Status__c, From__c, To__c, Overall_RAG__c, Createddate, Relationship_Summary__c, Summary_Quadrant1__c, Summary_Quadrant2__c, Summary_Quadrant3__c,
                            Summary_Quadrant4__c, Potential_Total_Score__c, Overall_Score__c, KPIs_Q1__c, KPIs_Q2__c, KPIs_Q3__c,
                            KPIs_Q4__c, Facts__c, RAG_Quadrant_1__c, RAG_Quadrant_2__c, RAG_Quadrant_3__c, RAG_Quadrant_4__c, Supplier__c from Score_Card__c where Id = : ScoreCardId];
            if(card.KPIs_Q1__c != null)
                ExistingKPIQ1.addall(card.KPIs_Q1__c.split(';'));
            if(card.KPIs_Q2__c != null)
                ExistingKPIQ2.addall(card.KPIs_Q2__c.split(';'));
            if(card.KPIs_Q3__c != null)
                ExistingKPIQ3.addall(card.KPIs_Q3__c.split(';'));
            if(card.KPIs_Q4__c != null)
                ExistingKPIQ4.addall(card.KPIs_Q4__c.split(';'));
        }
        if(card.Status__c == 'Published')
            Published = True;
        if(ScoreCardId != null)
            AccountId = card.Supplier__c;
            
        Supplier = [Select id, Name, Primary_Category__c, Owner.Email, Supplier_Key_Contact__r.name, Owner.Name, Supplier_Key_Contact__r.Email  from Account where Id = : AccountId];
        for(String KPIQ1 : ExistingKPIQ1){
            tempKPI = new List<String>();
            tempKPI.addall(KPIQ1.split(','));
            if(tempKPI[2] != 'null')
                selectedKPIsQ1.add(tempKPI[1] + ' (' + tempKPI[2] + ')' );
            else
                selectedKPIsQ1.add(tempKPI[1]);
        }
        for(String KPIQ2 : ExistingKPIQ2){
            tempKPI = new List<String>();
            tempKPI.addall(KPIQ2.split(','));
            if(tempKPI[2] != 'null')
                selectedKPIsQ2.add(tempKPI[1] + ' (' + tempKPI[2] + ')' );
            else
                selectedKPIsQ2.add(tempKPI[1]);
        }
        for(String KPIQ3 : ExistingKPIQ3){
            tempKPI = new List<String>();
            tempKPI.addall(KPIQ3.split(','));
            if(tempKPI[2] != 'null')
                selectedKPIsQ3.add(tempKPI[1] + ' (' + tempKPI[2] + ')' );
            else
                selectedKPIsQ3.add(tempKPI[1]);
        }
        for(String KPIQ4 : ExistingKPIQ4){
            tempKPI = new List<String>();
            tempKPI.addall(KPIQ4.split(','));
            if(tempKPI[2] != 'null')
                selectedKPIsQ4.add(tempKPI[1] + ' (' + tempKPI[2] + ')' );
            else
                selectedKPIsQ4.add(tempKPI[1]);
        }
    }
    catch(exception ee){
        // cannot insert exception as the method is on page load
    }    
    }
    
    /*
    * @description: This  method is used to redirect user to Error screen if he has no access on account
    * @param: None
    * @return Type: None
    */  
    public pagereference RedirectView(){
        PageReference pageRef;
         if(ScoreCardId != null){
            card = [Select id, Supplier__c from Score_Card__c where Id = : ScoreCardId];
         }
    // check if the user has read access to account or else give error
     try{       
        UserRecordAccess ura = [select RecordId, HasEditAccess, HasReadAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :card.Supplier__c];
        if (!(ura.HasReadAccess)){
            pageRef = Page.JJ_SRM_Error_Page;    
            pageRef.setRedirect(true);       
        }
    }
    catch(exception e){
         // cannot insert exception as the method is on page load
    }
        return pageRef; 
    }
}