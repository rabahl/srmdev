/**************************************************************************************
Apex Class Name  : JJ_SRM_scorecard_controller_view_Test
Version          : 1.0
Created Date     : 23rd November 2015
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Radhika Bahl        23rd November 2015        Original Version
  Sowmya Kodurupaka   15th February 2016        Modified Version   
*************************************************************************************/
@isTest(seeAllData=false)
private class JJ_SRM_scorecard_controller_view_Test{
    private static User userRec;
    private static List<Account> Acclist =  new List<Account>();
    private static Score_Card__c card;
    static testMethod void testSetup() {
        List<String> objlist = new List<String>();
        objlist.add('Account');
        JJ_SRM_TestDataSupplier.Createtriggerstatus(objlist);
        userRec = JJ_SRM_TestDataSupplier.createUser('SRM','System Administrator');
        Acclist = JJ_SRM_TestDataSupplier.CreateAccount(3,3);    
        card= JJ_SRM_TestDataSupplier.createScoreCard();
        card.Supplier__c=Acclist[0].Id;
        if(card!=null){
            insert card;
        }
    }
    static testMethod void getScoreCardTest() {
        Test.startTest();
        testSetup();
        ApexPages.StandardController sc = new ApexPages.StandardController(card);
        JJ_SRM_scorecard_controller_view scobj=new JJ_SRM_scorecard_controller_view();
        JJ_SRM_scorecard_controller_view obj=new JJ_SRM_scorecard_controller_view(sc);
        obj.ScoreCardId = card.id;
        obj.getScoreCard();
        obj.RedirectView();
        system.assertequals(obj.selectedKPIsQ1[0],'b (z)');
        system.assertequals(obj.selectedKPIsQ2[0],'d (y)');
        system.assertequals(obj.selectedKPIsQ3[0],'f (x)');
        system.assertequals(obj.selectedKPIsQ4[0],'h (q)');
        Test.stopTest();
    }
    
}