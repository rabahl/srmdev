/**
*      @author Mukta Sachdeva
*      @date   11/25/2015
       @description  Batch class to add user into chatter group - Global Services

        Modification Log    
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Mukta Sachdeva                  11/25/2015         Created   
        Dhruv Bhalodi                   29/01/2015         Modified to Add SRM User addition logic
*****/



global class JJ_UserGroupMemberUpdate implements Database.Batchable<sObject>, Database.Stateful {
    
    
    global List<CollaborationGroup> chatterGroups = [select id, Name from CollaborationGroup where name =:  label.Global_Service_Chatter_Group];
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        String profileNames = Label.Chatter_Group_Profiles;
        // Added logic to ensure SRM users assigned with permission set are added to announcement group
        List<String> permissionsetNames = Label.JJ_SRM_Permission_Sets.split(',');
        List<Id> SRMESPUsers = new List<Id>();
        List<PermissionSetAssignment> SRMPermissionsets = [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name in : permissionsetNames AND PermissionSet.ProfileId = null];
        for(PermissionSetAssignment Uid : SRMPermissionsets){
            SRMESPUsers.add(Uid.AssigneeId);    
        }
        List<string> profileallowed = profileNames.split(',',0);
        String query = 'SELECT Id,Assigned_to_Chatter__c,profile.name FROM user where  Assigned_to_Chatter__c = false and isactive = true  AND (profile.name in' ;
        query+=':profileallowed';
        // Added logic to ensure SRM users assigned with permission set are added to announcement group 
        query+=' OR Id in : SRMESPUsers)';
        if(Test.isRunningTest()){
            query+=' LIMIT 100';
        }
        return Database.getQueryLocator(query);
    }
   
  
    global void execute(Database.BatchableContext BC, List<user> users) {
      //  List<String> groups=new List<String>{'Global Services'};
        //List<CollaborationGroup> chatterGroups=[select id, Name from CollaborationGroup where name in :groups];
        
       map<id, id> membergroup = new map<id,id>();
       list<CollaborationGroupMember> Addedmember = [ Select id,CollaborationGroupId,MemberId from CollaborationGroupMember where CollaborationGroupId =: chatterGroups[0].id and  MemberId in :users ];
       
       for (CollaborationGroupMember cg: Addedmember )
            membergroup.put(cg.memberID, cg.CollaborationGroupId);
        
       
        List<CollaborationGroupMember> chatterGroupMembers=new List<CollaborationGroupMember>();
        for (User user : users)
        {
             if (!( membergroup.containskey(user.id))){
               CollaborationGroupMember cand = new CollaborationGroupMember(CollaborationGroupId=chatterGroups[0].id, MemberId = user.Id);
                chatterGroupMembers.add(cand);
            }
                user.Assigned_to_Chatter__c = true; 
        }
        try{
                database.insert(chatterGroupMembers,false);
                database.update(users,false);    
        }
        catch(exception e)
        { 
            System.debug('===>'+e.getmessage());
        }
    }  
     
    global void finish(Database.BatchableContext BC) {
    }
}