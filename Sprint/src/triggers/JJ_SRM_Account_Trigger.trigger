/**************************************************************************************
Apex Trigger Name: JJ_SRM_Account_Trigger
Version          : 1.0 
Created Date     : 14th October 2015 

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dhruv Bhalodi                  14th October 2014         Original Version
*************************************************************************************/

trigger JJ_SRM_Account_Trigger on Account (after insert,before update) {  
    Trigger_Status__c AccountSetting = Trigger_Status__c.getValues('Account');    
    if(AccountSetting != null && AccountSetting.Active__c == True){
       if(trigger.IsAfter && trigger.isInsert){
               JJ_SRM_Account_Trigger_Handler.CreateSupplierGovernance(Trigger.new);
       }
    }
}