/**
*       @author Dhruv Bhalodi
*       @date   01/04/2016
        @description  Trigger On Contact Object for SRM functinality

        Function: 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Dhruv Bhalodi                  01/04/2015          Created  
        
**/

// This Trigger is used for SRM contact functionality and needs to be merged with JJ_Contact before go Live
trigger JJ_SRM_Contact on Contact (Before Insert) {

 Trigger_Status__c ContactSetting = Trigger_Status__c.getValues('Contact');   
 if(ContactSetting != null && ContactSetting.Active__c == True){
 if(trigger.isbefore && trigger.isinsert && Label.JJ_SRM_External_Supplier_ProfileID.Contains(UserInfo.getProfileId())){
          JJ_SRM_Contact_Trigger_Handler.ChangeContactOwner(trigger.new);   
      }
 } 
}