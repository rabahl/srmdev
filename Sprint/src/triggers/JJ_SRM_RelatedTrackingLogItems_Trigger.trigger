trigger JJ_SRM_RelatedTrackingLogItems_Trigger on Related_Action_Log__c(after insert, after delete, after update) {
    Trigger_Status__c Related_Action_Log_Setting = Trigger_Status__c.getValues('Related_Action_Log__c');   
    if(Related_Action_Log_Setting != null && Related_Action_Log_Setting.Active__c == True){ 
        JJ_SRM_RelatedTracking_Trigger_Handler Triggerhandler = new JJ_SRM_RelatedTracking_Trigger_Handler();
        if (Trigger.isInsert) {
            Triggerhandler.InsertHandler_RelatedTracking(Trigger.new);  
        }

        if (Trigger.isDelete) {
            Triggerhandler.DeleteHandler_RelatedTracking(Trigger.old);    
        }

        if (Trigger.isUpdate) {
            Triggerhandler.UpdateHandler_RelatedTracking(Trigger.new,Trigger.oldMap,Trigger.newMap);    
        }
    }
}