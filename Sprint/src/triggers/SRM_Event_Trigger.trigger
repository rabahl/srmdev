trigger SRM_Event_Trigger on Event (after insert,after update, before delete) {
    Trigger_Status__c EventSetting = Trigger_Status__c.getValues('Event');   
    if(EventSetting != null && EventSetting.Active__c == True){
        if(trigger.isInsert||trigger.isUpdate)
            JJ_Event_trigger_handler.updateGovernanceMetrics(trigger.new,false);
        else if(trigger.isDelete&&trigger.isbefore)
            JJ_Event_trigger_handler.updateGovernanceMetrics(trigger.old,true);
    }   
}