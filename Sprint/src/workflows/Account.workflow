<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JJ_SRM_Notification_to_SRM_on_account_deactivation</fullName>
        <description>JJ_SRM_Notification to SRM on account deactivation</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SRM/JJ_SRM_Notify_SRM_on_Account_deactivation</template>
    </alerts>
    <rules>
        <fullName>JJ_SRM_Notification to SRM when account is deactivated</fullName>
        <actions>
            <name>JJ_SRM_Notification_to_SRM_on_account_deactivation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED( Active__c), Active__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
