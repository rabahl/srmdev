<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>JJ_SRM_PopulateKPISearchable</fullName>
        <actions>
            <name>JJ_SRM_PopulateKPISearchable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for SRM US-4645, to populate KPI Searchable field with KPI Library Level 1-3</description>
        <formula>ISNEW()  || ( ISCHANGED( KPI_Library_Level_1__c ) || ISCHANGED(  KPI_Library_Sub_Library_Level_2__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <fieldUpdates>
        <fullName>JJ_SRM_PopulateKPISearchable</fullName>
        <description>Populates  KPI Searchable field using KPI Library Levels 1-2</description>
        <field>KPI_Searchable__c</field>
        <formula>TEXT(KPI_Library_Level_1__c) &amp;  &apos;,&apos; &amp; TEXT(KPI_Library_Sub_Library_Level_2__c)</formula>
        <name>JJ_SRM_PopulateKPISearchable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
