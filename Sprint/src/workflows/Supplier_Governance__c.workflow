<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Key_on_Supplier_Governance</fullName>
        <field>Governance_Unique_ID__c</field>
        <formula>Governance__r.Name +  Text(Year__c)  +  Text(Governance__r.Segment__c) +   Supplier__r.Id  +  Governance__r.Id</formula>
        <name>Update Unique Key on Supplier Governance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JJ SRM Update Unique key on Supplier Governance</fullName>
        <actions>
            <name>Update_Unique_Key_on_Supplier_Governance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created to populate Unique key on Supplier Governance which will help identify records during creation from Governance List view</description>
        <formula>ISNEW() || ISCHANGED(Governance__c) || ISCHANGED(Supplier__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
