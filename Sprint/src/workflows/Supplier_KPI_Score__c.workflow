<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_SRM_UniqueKeyOnKPIScore</fullName>
        <description>Created for US-4655, to populate the Supplier KPI Id plus Period as unique key</description>
        <field>Unique_Key__c</field>
        <formula>Supplier_KPI__c  &amp; TEXT(Period__c)</formula>
        <name>JJ_SRM_UniqueKeyOnKPIScore</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JNJ_SRM_Update_KPI_Score_Record_Type</fullName>
        <description>This field update is used to modify KPI Score record type to display threshold values on layout</description>
        <field>RecordTypeId</field>
        <lookupValue>KPI_Score_Detail</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>JNJ SRM Update KPI Score Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JJ_SRM_UniqueKeyOnKPIScore</fullName>
        <actions>
            <name>JJ_SRM_UniqueKeyOnKPIScore</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for US-4655, to populate Unique Key - Supplier KPI Id plus Period</description>
        <formula>ISNEW() || (ISCHANGED( Period__c )|| ISCHANGED(Supplier_KPI__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JNJ SRM Update record type of score to detail</fullName>
        <actions>
            <name>JNJ_SRM_Update_KPI_Score_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow updates the record type of KPI Score record to show threshold values of KPI which were not present at the time of creation</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
