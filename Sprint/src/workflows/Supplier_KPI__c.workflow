<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_SRM_Update_Unique_Identifier_KPI</fullName>
        <field>Unique_Identifier__c</field>
        <formula>Supplier_Name__c + KPI_Master__c + TEXT(Evaluation_Period__c)</formula>
        <name>JJ_SRM_Update Unique Identifier KPI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JJ_SRM_Update Unique Identifier</fullName>
        <actions>
            <name>JJ_SRM_Update_Unique_Identifier_KPI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Supplier_KPI__c.KPI_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
