<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JJ_SRM_Iternal_User_Activation_Email_Alert</fullName>
        <description>JJ SRM Iternal User Activation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SRM/Internal_SRM_Welcome_Template</template>
    </alerts>
</Workflow>
